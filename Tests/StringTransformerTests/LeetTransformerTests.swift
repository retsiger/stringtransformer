//
//  LeetTransformerTests.swift
//  
//
//  Created by Joël Brogniart on 24/07/2021.
//

import Testing
@testable import StringTransformer

@Suite("Leet transformer test")
struct LeetTransformerTests {
    let transformer = LeetTransformer()
    let name = String.Transformer.leet
    let purity = true

    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }

    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "abc defg hijkl mnopqr stuvwxy z"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }

    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }

    @Test("Leet transform you should return j00")
    func leetTransformYouShouldReturnj00() {
        let input = "you"
        let expectedString = "j00"
        #expect(transformer.transform(input) == expectedString)
    }
    
    @Test("Leet transform fear should return ph33r")
    func leetTransformFearShouldReturnPh33r() {
        let input = "fear"
        let expectedString = "ph33r"
        #expect(transformer.transform(input) == expectedString)
    }
    
    @Test("Leet transform ate should return 8")
    func leetTransformAteShouldReturn8() {
        let input = "ate"
        let expectedString = "8"
        #expect(transformer.transform(input) == expectedString)
    }
    
    @Test("Leet transform you ate fear should return j00 8 ph33r")
    func leetTransformYouAteFearShouldReturnJ008ph33r() {
        let input = "you ate fear"
        let expectedString = "j00 8 ph33r"
        #expect(transformer.transform(input) == expectedString)
    }
    
    @Test("Leet transform string ended by s should return string ended by z")
    func leetTransformEndingSShouldReturnEndingZ() {
        let input = "bus"
        let expectedString = "buz"
        #expect(transformer.transform(input) == expectedString)
    }
        
    @Test("Leet transform bite should return 8!+3")
    func leetTransform_BitesShouldReturnStrangeBites() {
        let input = "Bite"
        let expectedString = "8!+3"
        #expect(transformer.transform(input) == expectedString)
    }
    
    @Test("Leet transform Sox should return S0><")
    func test_LeetTransform_Sox_ShouldReturnSZeroGreaterthanLowerthan() {
        let input = "Sox"
        let expectedString = "S0><"
        #expect(transformer.transform(input) == expectedString)
    }
}
