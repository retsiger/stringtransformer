//
//  MorseTransformerTests.swift
//  
//
//  Created by Joël Brogniart on 19/07/2021.
//

import Testing
@testable import StringTransformer

@Suite("Morse transformer test")
struct MorseTransformerTests {
    let transformer = MorseTransformer()
    let name = String.Transformer.morse
    let purity = true

    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }

    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "abc defg hijkl mnopqr stuvwxy z"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }

    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }

    @Test("Morse ransform EIS should return . .. ...")
    func morseTransformEISShouldBeOneDotTwoDotsThreeDots() {
        let est = "eis"
        let expectedResult = ". .. ..."
        #expect(transformer.transform(est) == expectedResult)
    }
    
    @Test("Morse transform lowercased string should return uppercased string")
    func morseTransformLowerCasedShouldEqualUpperCased() {
       let lowerCased = "abcdefghijklmnopqrstuvwxyz"
       let upperCased = lowerCased.uppercased()
        #expect(transformer.transform(lowerCased) == transformer.transform(upperCased))
   }
}
