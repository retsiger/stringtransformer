//
//  MixedCaseTransformerTests.swift
//
//
//  Created by Joël Brogniart on 20/01/2022.
//

import Testing
@testable import StringTransformer

@Suite("MixedCase transformer test")
struct MixedCaseTransformerTests {
    let transformer = MixedCaseTransformer()
    let name = String.Transformer.mixedcase
    let purity = false

    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }

    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "abc defg hijkl mnopqr stuvwxy z"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }

    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }

    @Test("MixedCase transform long string should return different string")
    func transformLongTextShouldNotIdenticalToOriginal() {
        let inputWord = "abcdefghijklmnopqzrstuvwxyz"
        let transformedWord = transformer.transform(inputWord)
        #expect(inputWord != transformedWord)
    }
}
