//
//  HexlifyTransformerTests.swift
//  
//
//  Created by Joël Brogniart on 20/07/2021.
//

import Testing
@testable import StringTransformer

@Suite("Hexlify transformer test")
struct HexlifyTransformerTests {
    let transformer = HexlifyTransformer()
    let name = String.Transformer.hexlify
    let purity = true

    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }

    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "abc defg hijkl mnopqr stuvwxy z"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }

    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }


    @Test("Hexlify transform ab string should return 6162")
    func hexlifyTransformABShouldBeHexadecimal0x6162() {
        let abString = "ab"
        let abHexadecimalString = "6162"
        #expect(transformer.transform(abString) == abHexadecimalString)
    }
    
    @Test("Hexlify transform hammer and sickle character should return e298bad")
    func hexlifyTransform_HammerAndSickleShouldBeHexadecimal0xE298AD() {
        let hammerAndSickleString = "☭"
        let hammerAndSickleHexadecimalString = "e2" + "98" + "ad"
        #expect(transformer.transform(hammerAndSickleString) == hammerAndSickleHexadecimalString)
    }
}
