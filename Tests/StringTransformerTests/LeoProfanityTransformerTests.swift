//
//  LeoProfanityTransformerTests.swift
//  StringTransformer
//
//  Created by Joël Brogniart on 27/12/2024.
//
import Testing
@testable import StringTransformer

@Suite("LeoProfanity transformer test")
struct LeoProfanityTransformerTests {
    let transformer = LeoProfanityTransformer()
    let name = String.Transformer.profanity
    let purity = true
    
    
    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }
    
    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "abc ass fuck bitch stuvwxy z"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }
    
    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }
    
    @Test("Transform should returm original string if there is no profanity world (English)")
    func transformShouldReturnOriginalStringIfThereIsNoProfanityEN() {
        let original = "I have 2 eyes."
        let expected = original
        #expect(transformer.transform(original) == expected)
    }
    
    @Test("Transform should returm original string if there is no profanity world (French)")
    func transformShouldReturnOriginalStringIfThereIsNoProfanityFR() {
        let original = "J'ai deux yeux."
        let expected = original
        #expect(transformer.transform(original) == expected)
    }
    
    @Test("Transform should replace profanity with * (English)", arguments:
            zip(["I have boob, etc.",
                 "2g1c",
                 "zoophilia",
                 "lorem 2g1c ipsum",
                 "lorem zoophilia ipsum"],
                ["I have ****, etc.",
                 "****",
                 "*********",
                 "lorem **** ipsum",
                 "lorem ********* ipsum"]))
    func transformShouldReplaceProfanityWithAsteriskEN(_ original: String, _ expected: String) {
        #expect(transformer.transform(original) == expected)
    }
    
    // Test could fail for "g@rce" as it is difficult to infer French language from
    // this word alone.
    @Test("Transform should replace profanity with * (French)", arguments:
            zip(["J'ai mal au cul.",
                 "empafé",
                 "g@rce",
                 "Tu n'es rien qu'une g@rc3.",
                 "lorem empafé ipsum"],
                ["J'ai mal au ***.",
                 "******",
                 "*****",
                 "Tu n'es rien qu'une *****.",
                 "lorem ****** ipsum"]))
    func transformShouldReplaceProfanityWithAsteriskFR(_ original: String, _ expected: String) {
        #expect(transformer.transform(original) == expected)
    }
    
    @Test("Transform should detect case sensitive (English)")
    func transformShouldDetectSensitiveEN() {
        let original = "I have BoOb."
        let expected = "I have ****."
        #expect(transformer.transform(original) == expected)
    }

    @Test("Transform should detect case sensitive (French)")
    func transformShouldDetectSensitiveFR() {
        let original = "J'ai mal au cUl."
        let expected = "J'ai mal au ***."
        #expect(transformer.transform(original) == expected)
    }

    @Test("Transform should detect multiple occurences (English)")
    func transformShouldDetectMultipleOccurencesEN() {
        let original = "I have boob,boob, ass, and etc."
        let expected = "I have ****,****, ***, and etc."
        #expect(transformer.transform(original) == expected)
    }

    @Test("Transform should detect multiple occurences (French)")
    func transformShouldDetectMultipleOccurencesFR() {
        let original = "J'ai un cul, une bite et des couilles etc."
        let expected = "J'ai un ***, une **** et des ******** etc."
        #expect(transformer.transform(original) == expected)
    }

    @Test("Transform should not detect unspaced words (English)")
    func transformShouldNotDetectUnspacedWordsEN() {
        let original = "Buy classic watches online."
        let expected = original
        #expect(transformer.transform(original) == expected)
    }

    @Test("Transform should not detect unspaced words (French)")
    func transformShouldNotDetectUnspacedWordsFR() {
        let original = "Il est acculé à la faillite et habite à la rue."
        let expected = original
        #expect(transformer.transform(original) == expected)
    }

    @Test("Transform should detect multi-length-space and multi-space (English)", arguments:
            zip(["I  hav   ,e BoOb,  ", ",I h  a.   v e BoOb."],
                ["I  hav   ,e ****,  ", ",I h  a.   v e ****."]))
    func transformShouldDetectMultiLengthSpaceAndMultiSpaceEN(_ original: String, _ expected: String) {
        #expect(transformer.transform(original) == expected)
    }

    @Test("Transform should detect multi-length-space and multi-space (French)", arguments:
            zip(["J'  ai   ,un cul,  ", ",J '  a.   i un cul."],
                ["J'  ai   ,un ***,  ", ",J '  a.   i un ***."]))
    func transformShouldDetectMultiLengthSpaceAndMultiSpaceFR(_ original: String, _ expected: String) {
        #expect(transformer.transform(original) == expected)
    }


}
