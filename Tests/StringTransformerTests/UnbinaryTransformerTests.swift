//
//  UnbinaryTransformerTests.swift
//  
//
//  Created by Joël Brogniart on 19/07/2021.
//

import Testing
@testable import StringTransformer

@Suite("Unbinary transformer test")
struct UnbinaryTransformerTests {
    let transformer = UnbinaryTransformer()
    let name = String.Transformer.unbinary
    let purity = true

    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }

    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "abc defg hijkl mnopqr stuvwxy z"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }

    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }

    @Test("Unbinary transform 0110000101100010 should return ab")
    func unbinaryTransform_Binary0x6162_ShouldBeab() {
        let abString = "ab"
        let abBinaryString = "01100001" + "01100010"
        #expect(transformer.transform(abBinaryString) == abString)
    }
    
    @Test("Unbinary transform 111000101001100010101101 should return ☭")
    func unbinaryTransform_Binary0xE298AD_ShouldBeHammerAndSickle() {
        let hammerAndSickleBinaryString = "11100010" + "10011000" + "10101101"
        let hammerAndSickleString = "☭"
        #expect(transformer.transform(hammerAndSickleBinaryString) == hammerAndSickleString)
    }
    
    @Test("Unbinary transform 111000101001100010101101 with garbage characters should return ☭")
    func unbinaryTransform_Binary0xE298ADWithGarbageCharacters_ShouldBeHammerAndSickle() {
        let hammerAndSickleBinaryString = "111000 10" + "100?11000" + "aeiouç10101101"
        let hammerAndSickleString = "☭"
        #expect(transformer.transform(hammerAndSickleBinaryString) == hammerAndSickleString)
    }
}
