//
//  AOLTransformerTests.swift
//  
//
//  Created by Joël Brogniart on 08/09/2021.
//

import Testing
@testable import StringTransformer

@Suite("AOL transformer test")
struct AOLTransformerTests {
    let transformer = AOLTransformer()
    let name = String.Transformer.aol
    let purity = false
    
    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }
    
    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "abc defg hijkl mnopqr stuvwxy z"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }
    
    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }


    @Test("Transformed OneTwoFour string should begin with 124")
    func aOLTransformedOneTwoFourShouldBeginWith124() {
        let input = "one two four"
        let transformed = transformer.transform(input)
        let expectedStart = "1 2 4"
        let transformedStart = String(transformed.prefix(expectedStart.count))
        #expect(expectedStart == transformedStart)
    }

    @Test("AOL transformed string should end with three smileys")
    func aolTransformedStringShouldEndWithThreeSmileys()  {
        let AOLSmileys = ["<3", ":)", ":-)", ":D", ":-D"]
        let input = "one two four"
        let transformed = transformer.transform(input)
        var found = false
        _ = AOLSmileys.map { smiley in
            let threeSmileys = String(repeating: smiley, count: 3)
            if threeSmileys == String(transformed.suffix(threeSmileys.count)) {
                found = true
            }
        }
        #expect(found, "Three smileys not found at the end of \(transformed)")
    }

    @Test("Transformed string should not contain commas")
    func aolTransformedStringShouldNotContainComma() {
        let input = "One, two, three, four"
        let transformed = transformer.transform(input)
        #expect(transformed.contains(",") == false)
    }

    @Test("Transformed string should not contain quote")
    func aolTransformedStringShouldNotContainQuote()  {
        let input = "One' two' three' four"
        let transformed = transformer.transform(input)
        #expect(transformed.contains("'") == false)
    }
}
