//
//  UndupTransformerTests.swift
//  
//
//  Created by Joël Brogniart on 13/07/2021.
//

import Testing
@testable import StringTransformer

@Suite("Undup transformer test")
struct UndupTransformerTests {
    let transformer = UndupTransformer()
    let name = String.Transformer.undup
    let purity = true

    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }

    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "abc defg hijkl mnopqr stuvwxy z"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }

    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }


    @Test("Undup transform should keep first character of sequence of two identical characters")
    func undupTransformeShouldKeepFirstLetterOfSequenceOfTwoIdenticalLetters() {
        let stringWithDoubledLetters = "oonnee"
        let expectedResult = "one"
        #expect(transformer.transform(stringWithDoubledLetters) == expectedResult)
    }
    
    @Test("Undup transform should keep first character of sequence of0 identical characters")
    func undupTransformeShouldKeepFirstLetterOfSequenceOfIdenticalLetters()  {
        let stringWithDuplicatedLetters =
        String(repeating: "0", count: 100)
        + String(repeating: "1", count: 100)
        + String(repeating: "2", count: 100)
        + String(repeating: "3", count: 100)
        + String(repeating: "4", count: 100)
        + String(repeating: "5", count: 100)
        + String(repeating: "6", count: 100)
        + String(repeating: "7", count: 100)
        + String(repeating: "8", count: 100)
        + String(repeating: "9", count: 100)
        + String(repeating: "!", count: 100)
        let expectedResult = "0123456789!"
        #expect(transformer.transform(stringWithDuplicatedLetters) == expectedResult)
    }
}
