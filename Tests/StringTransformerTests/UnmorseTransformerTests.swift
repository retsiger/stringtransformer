//
//  UnmorseTransformerTests.swift
//  
//
//  Created by Joël Brogniart on 07/09/2021.
//

import Testing
@testable import StringTransformer

@Suite("Unmorse transformer test")
struct UnmorseTransformerTests {
    let transformer = UnmorseTransformer()
    let name = String.Transformer.unmorse
    let purity = true

    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }

    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "abc defg hijkl mnopqr stuvwxy z"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }

    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }

    @Test("Unmorse transform ..- should return U")
    func unmorseTransformTransformDotDotDashShouldbeU() {
        let dotDotDash = "..-"
        let expectedResult = "U"
        #expect(transformer.transform(dotDotDash) == expectedResult)

    }
    
    @Test("Unmorse transform . .. ... should return EIS")
    func unmorseTransformOneDotTwoDotsThreeDotsShouldBeEIS() {
        let oneDotTwoDotsThreeDots = ". .. ..."
        let expectedResult = "EIS"
        #expect(transformer.transform(oneDotTwoDotsThreeDots) == expectedResult)
    }
}
