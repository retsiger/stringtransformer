//
//  Supa1337Transformer.swift
//  
//
//  Created by Joël Brogniart on 24/07/2021.
//

import Testing
@testable import StringTransformer

@Suite("Supa1337 transformer test")
struct Supa1337TransformerTests {
    let transformer = Supa1337Transformer()
    let name = String.Transformer.supa1337
    let purity = true

    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }

    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "abc defg hijkl mnopqr stuvwxy z"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }

    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }

    @Test("Supa1337 transform you should return j00")
    func supa1337TransformYouShouldReturnj00() {
        let input = "you"
        let expectedString = "j00"
        #expect(transformer.transform(input) == expectedString)
    }
    
    @Test("Supa1337 transform huluberlu should return |-||_|1|_||>3|21|_|")
    func supa1337TransformHuluberluShouldReturnStrangeHuluberlu() {
        let input = "huluberlu"
        let expectedString = "|-||_|1|_||>3|21|_|"
        #expect(transformer.transform(input) == expectedString)
    }
    
    @Test("Supa1337 transform ate should return 8")
    func supa1337TransformAteShouldReturn8() {
        let input = "ate"
        let expectedString = "8"
        #expect(transformer.transform(input) == expectedString)
    }
}
