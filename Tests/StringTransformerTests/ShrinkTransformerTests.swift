//
//  ShrinkTransformerTests.swift
//  
//
//  Created by Joël Brogniart on 16/09/2021.
//

import Testing
@testable import StringTransformer

@Suite("Shrink transformer test")
struct ShrinkTransformerTests {
    let transformer = ShrinkTransformer()
    let name = String.Transformer.shrink
    let purity = true

    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }

    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "abc defg hijkl mnopqr stuvwxy z"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }

    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }

    @Test("Shrink transform one character string should not be changed")
    func shrinkTransformOneLetterWordsShouldNotBeChanged() {
        let input = "a"
        #expect(transformer.transform(input) == input)
    }
    
    @Test("Shrink transform two characters string should not be changed")
    func shrinkTransform_TwoLettersWordsShouldNotBeChanged() {
        let input = "ab"
        #expect(transformer.transform(input) == input)
    }
    
    @Test("Shrink transform three characters string should not be changed")
    func shrinkTransform_ThreeLetterWordsShouldNotBeChanged() {
        let input = "abc"
        #expect(transformer.transform(input) == input)
    }
    
    @Test("Shrink transform four characters string should not be changed")
    func shrinkTransform_FourLetterWordsShouldNotBeChanged() {
        let input = "abcd"
        #expect(transformer.transform(input) == input)
    }
    
    @Test("Shrink transform five characters string should be changed")
    func shrinkTransform_FiveLetterWordsShoulBeChanged() {
        let input = "abcde"
        #expect(transformer.transform(input) != input)
    }
    
    @Test("Shrink transform five words string should give five words")
    func shrinkTransformFiveWordsShouldGiveFiveWords() {
        let input = "abcde bcdef cdefg defgh efghi"
        let transformed = transformer.transform(input)
        var inputWords = [String]()
        input.enumerateSubstrings(in: input.startIndex..<input.endIndex, options: .byWords) { (substring, _, _, _) -> () in
            if substring != nil {
                inputWords.append(substring!)
            }
        }
        var transformedWords = [String]()
        transformed.enumerateSubstrings(in: transformed.startIndex..<transformed.endIndex, options: .byWords) { (substring, _, _, _) -> () in
            if substring != nil {
                transformedWords.append(substring!)
            }
        }
        #expect(inputWords.count == 5)
        #expect(transformedWords.count == 5)
    }

    @Test("Shrink transformed first character of string should not be changed")
    func shrinkTransformFirstLetterOfWordShouldNotBeChanged() {
        let input = "abcde bcdef cdefg defgh"
        let transformed = transformer.transform(input)
        var inputWords = [String]()
        input.enumerateSubstrings(in: input.startIndex..<input.endIndex, options: .byWords) { (substring, _, _, _) -> () in
            inputWords.append(substring!)
        }
        var transformedWords = [String]()
        transformed.enumerateSubstrings(in: transformed.startIndex..<transformed.endIndex, options: .byWords) { (substring, _, _, _) -> () in
            transformedWords.append(substring!)
        }
        for index in 0..<4 {
            #expect(inputWords[index].prefix(1) == transformedWords[index].prefix(1))
        }
    }

    @Test("Shrink transformed last character of string should not be changed")
    func shrinkTransformLastLetterOfWordShouldNotBeChanged() {
        let input = "abcde bcdef cdefg defgh"
        let transformed = transformer.transform(input)
        var inputWords = [String]()
        input.enumerateSubstrings(in: input.startIndex..<input.endIndex, options: .byWords) { (substring, _, _, _) -> () in
            inputWords.append(substring!)
        }
        var transformedWords = [String]()
        transformed.enumerateSubstrings(in: transformed.startIndex..<transformed.endIndex, options: .byWords) { (substring, _, _, _) -> () in
            transformedWords.append(substring!)
        }
        for index in 0..<4 {
            #expect(inputWords[index].suffix(1) == transformedWords[index].suffix(1))
        }
    }

    @Test("Shrink transformed of internationalization should be i18n")
    func shrinkTransformInternationalizationShouldBeI18n() {
        let inputWord = "internationalization"
        let expected = "i18n"
        #expect(transformer.transform(inputWord) == expected)
    }
}
