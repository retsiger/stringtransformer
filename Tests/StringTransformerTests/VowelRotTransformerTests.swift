//
//  VowelRotTransformerTests.swift
//  
//
//  Created by Joël Brogniart on 22/09/2021.
//

import Testing
@testable import StringTransformer

@Suite("VowelRot transformer test")
struct VowelRotTransformerTests {
    let transformer = VowelRotTransformer()
    let name = String.Transformer.vowelrot
    let purity = true

    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }

    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "abc defg hijkl mnopqr stuvwxy z"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }

    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }

    @Test("VowelRot transform string without vowel should return same string")
    func vowelRotTransformeStringWithoutVowelShouldNotBeModified() {
        let input = "bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ!"
        #expect(transformer.transform(input) == input)
    }
    
    @Test("VowelRot transform string with vowels should rotate vowels")
    func vowelRotTransformeStringShouldRotateVowels() {
        let input = "aeiou AEIOU"
        let expected = "eioua EIOUA"
        #expect(transformer.transform(input) == expected)
    }
}
