//
//  ScrambleTransformerTests.swift
//  
//
//  Created by Joël Brogniart on 02/09/2021.
//

import Testing
@testable import StringTransformer

@Suite("Scamble transformer test")
struct ScrambleTransformerTests {
    let transformer = ScrambleTransformer()
    let name = String.Transformer.scramble
    let purity = false

    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }

    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "abc defg hijkl mnopqr stuvwxy z"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }

    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }

    @Test("Scramble transform one character words should return same words")
    func scrambleTransformOneLetterWordsShouldNotBeChanged() {
        let input = "a b c d"
        #expect(transformer.transform(input) == input)
    }
    
    @Test("Scramble transform two characters words should return same words")
    func scrambleTransformTwoLettersWordsShouldNotBeChanged() {
        let input = "ab bc cd de"
        #expect(transformer.transform(input) == input)
    }
    
    @Test("Scramble transform three characters words should return same words")
    func scrambleTransformThreeLetterWordsShouldNotBeChanged() {
        let input = "abc bcd cde def"
        #expect(transformer.transform(input) == input)
    }
    
    @Test("Scramble transform four words should return four words")
   func scrambleTransformFourWordsShouldGiveFourWords() {
        let input = "abcd bcde cdef defg"
        let transformed = transformer.transform(input)
        var inputWords = [String]()
        input.enumerateSubstrings(in: input.startIndex..<input.endIndex, options: .byWords) { (substring, _, _, _) -> () in
            if substring != nil {
                inputWords.append(substring!)
            }
        }
        var transformedWords = [String]()
        transformed.enumerateSubstrings(in: transformed.startIndex..<transformed.endIndex, options: .byWords) { (substring, _, _, _) -> () in
            if substring != nil {
                transformedWords.append(substring!)
            }
        }
        #expect(inputWords.count == 4)
        #expect(transformedWords.count == 4)
    }

    @Test("Scramble transform for some strings should return same strings")
    func scrambleTransformForSomeStringsTransformedShouldBeEqualToOriginal() {
        let input = "zéééz zèèèz zàààz zùùùz"
        let transformed = transformer.transform(input)
        #expect(input == transformed)
    }

    @Test("Scramble transform first character of words should not be changed")
    func scrambleTransform_FirstLetterOfWord_ShouldNotBeChanged() {
        let input = "abcd bcde cdef defg"
        let transformed = transformer.transform(input)
        var inputWords = [String]()
        input.enumerateSubstrings(in: input.startIndex..<input.endIndex, options: .byWords) { (substring, _, _, _) -> () in
            inputWords.append(substring!)
        }
        var transformedWords = [String]()
        transformed.enumerateSubstrings(in: transformed.startIndex..<transformed.endIndex, options: .byWords) { (substring, _, _, _) -> () in
            transformedWords.append(substring!)
        }
        for index in 0..<4 {
            #expect(inputWords[index].prefix(1) == transformedWords[index].prefix(1))
        }
    }

    @Test("Scramble transform last character of words should not be changed")
   func scrambleTransformLastLetterOfWordShouldNotBeChanged() {
        let input = "abcd bcde cdef defg"
        let transformed = transformer.transform(input)
        var inputWords = [String]()
        input.enumerateSubstrings(in: input.startIndex..<input.endIndex, options: .byWords) { (substring, _, _, _) -> () in
            inputWords.append(substring!)
        }
        var transformedWords = [String]()
        transformed.enumerateSubstrings(in: transformed.startIndex..<transformed.endIndex, options: .byWords) { (substring, _, _, _) -> () in
            transformedWords.append(substring!)
        }
        for index in 0..<4 {
            #expect(inputWords[index].suffix(1) == transformedWords[index].suffix(1))
        }
    }

    @Test("Scramble transform long words should rarely be same words")
    func scrambleTransformLongTransformedWordShouldRarelyBeIdenticalToOriginal() {
        let inputWord = "abcdefghijklmnopqzrstuvwxyz"
        let transformedWord = transformer.transform(inputWord)
        #expect(inputWord != transformedWord)
    }
}
