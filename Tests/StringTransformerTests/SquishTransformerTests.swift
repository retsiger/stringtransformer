//
//  SquishTransformerTests.swift
//  
//
//  Created by Joël Brogniart on 13/07/2021.
//

import Testing
@testable import StringTransformer

@Suite("Squish transformer test")
struct SquishTransformerTests {
    let transformer = SquishTransformer()
    let name = String.Transformer.squish
    let purity = true

    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }

    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "abc defg hijkl mnopqr stuvwxy z"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }

    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }


    @Test("Squish transform spaces in string should be removed")
    func squishTransformSpaceShouldBeRemoved() {
        let stringWithSpace = "one two"
        let expectedResult = "onetwo"
        #expect(transformer.transform(stringWithSpace) == expectedResult)
    }
    
    @Test("Squish transform non breaking spaces in string should be removed")
    func squishTransformNonBreakingSpaceShouldBeRemoved() {
        let stringWithNonBreakingSpace = "one\u{A0}two"
        let expectedResult = "onetwo"
        #expect(transformer.transform(stringWithNonBreakingSpace) == expectedResult)
    }
    
    @Test("Squish transform tabulations in string should be removed")
    func squishTransformTabulationShouldBeRemoved() {
        let stringWithTabulation = "one\ttwo"
        let expectedResult = "onetwo"
        #expect(transformer.transform(stringWithTabulation) == expectedResult)
    }
    
    @Test("Squish transform line feed in string should be removed")
    func squishTransformLineFeedShouldBeRemoved() {
        let stringWithNewline = "one\ntwo"
        let expectedResult = "onetwo"
        #expect(transformer.transform(stringWithNewline) == expectedResult)
    }
    
    @Test("Squish transform carriage return in string should be removed")
    func squishTransformCarriageReturnShouldBeRemoved() {
        let stringWithCarriageReturn = "one\rtwo"
        let expectedResult = "onetwo"
        #expect(transformer.transform(stringWithCarriageReturn) == expectedResult)
    }
    
    @Test("Squish transform punctuations in string should not be removed")
    func squishTransformPunctuationShouldNotBeRemoved() {
        let stringWithPunctuation = "one, two !"
        let expectedResult = "one,two!"
        #expect(transformer.transform(stringWithPunctuation) == expectedResult)
    }
}
