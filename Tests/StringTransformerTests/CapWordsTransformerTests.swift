//
//  CapWordsTransformerTests.swift
//  
//
//  Created by Joël Brogniart on 21/09/2021.
//

import Testing
@testable import StringTransformer

@Suite("CapWords transformer test")
struct CapWordransformerTests {
    let transformer = CapWordsTransformer()
    let name = String.Transformer.capwords
    let purity = true

    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }

    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "abc defg hijkl mnopqr stuvwxy z"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }

    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }

    @Test("CapWords transformed  string should be uppercased")
    func capWordsTransformedStringShouldBeUppercased() {
        let input = "Text"
        var expected: String
        if #available(macOS 10.11, iOS 9, *) {
            expected = input.localizedCapitalized
        } else {
            expected = input.capitalized
        }
        #expect(transformer.transform(input) == expected)
    }
}
