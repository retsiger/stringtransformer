//
//  UnhexlifyTransformerTests.swift
//  
//
//  Created by Joël Brogniart on 20/07/2021.
//

import Testing
@testable import StringTransformer

@Suite("Unhexlify transformer test")
struct UnhexlifyTransformerTests {
    let transformer = UnhexlifyTransformer()
    let name = String.Transformer.unhexlify
    let purity = true

    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }

    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "abc defg hijkl mnopqr stuvwxy z"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }

    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }

    @Test("Unlexify transform 6162 should return ab")
    func unhexlifyTransformHexadecimal0x6162ShouldBeab() {
        let abString = "ab"
        let abBinaryString = "61" + "62"
        #expect(transformer.transform(abBinaryString) == abString)
    }
    
    @Test("Unlexify transform e298ad should return ☭")
    func unhexlifyTransformHexadecimal0xE298ADShouldBeHammerAndSickle() {
        let hammerAndSickleHexadecimalString = "e2" + "98" + "ad"
        let hammerAndSickleString = "☭"
        #expect(transformer.transform(hammerAndSickleHexadecimalString) == hammerAndSickleString)
    }
    
    @Test("Unlexify transform e298ad with garbage characters should return ☭")
    func unhexlifyTransformHexadecimal0xE298ADWithGarbageCharactersShouldBeHammerAndSickle() {
        let hammerAndSickleHexadecimalString = "e 2" + "9?8" + "zkZK!ad"
        let hammerAndSickleString = "☭"
        #expect(transformer.transform(hammerAndSickleHexadecimalString) == hammerAndSickleString)
    }
    
    @Test("Unlexify transform e298ad or E298AD should return ☭")
    func unhexlifyTransformHexadecimal0xE298ADShouldBeGiveSameResultForLowerAndUppercase() {
        let hammerAndSickleHexadecimalStringLower = "e2" + "98" + "ad"
        let hammerAndSickleHexadecimalStringUpper = "E2" + "98" + "AD"
        #expect(transformer.transform(hammerAndSickleHexadecimalStringLower) == transformer.transform(hammerAndSickleHexadecimalStringUpper))
    }
}
