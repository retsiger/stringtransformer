//
//  HebrewTransformerTests.swift
//
//  Created by Joël Brogniart on 12/07/2021.
//

import Testing
@testable import StringTransformer

@Suite("Hebrew transformer test")
struct HebrewTransformerTests {
    let transformer = HebrewTransformer()
    let name = String.Transformer.hebrew
    let purity = true

    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }

    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "abc defg hijkl mnopqr stuvwxy z"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }

    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }

    @Test("Hebrew transform accented letters should be suppressed")
 func hebrewTransformAccentedVowelsShouldBeDeleted() {
        let stringWithAccentedLetters = "Les élèves français éte\u{301} niñão !"
        let expectedResult = "Ls lvs frnçs t nñ !"
     #expect(transformer.transform(stringWithAccentedLetters) == expectedResult)
    }    
}
