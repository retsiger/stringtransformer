//
//  SchtroumpfTransformerTest.swift
//  StringTransformer
//
//  Created by Joël Brogniart on 19/11/2024.
//

import Testing
@testable import StringTransformer

@Suite("Schtroumpf transformer test")
struct SchtroumpfTransformerTest {
    let transformer = SchtroumpfTransformer()
    let name = String.Transformer.schtroumpf
    let purity = false
    
    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }
    
    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "Aller à la pêche aux moules"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }
    
    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }
    
    @Test("Transform non French text should give the same text")
    func transformNonFrenchTextShouldGiveTheSameTest() {
        let nonFrenchString = "aaaa bbbb cccc dddd eeee ffff gggg hhhh iiii jjjj kkkk llll"
        #expect(transformer.transform(nonFrenchString) == nonFrenchString)
    }

    @Test("Transform French noun should sometimes give schtroumpf")
    func transformNonFrenchNounShouldSometimesGiveSchtroumpf() {
        let frenchNoun = "Une arbalète est ici."
        let expected = "Une schtroumpf est ici."
        var schtroumpf = false
        for _ in 1...20 {
            if transformer.transform(frenchNoun) == expected {
                schtroumpf = true
                break
            }
        }
        #expect(schtroumpf == true)
    }

    @Test("Transform French noun with initial capital should sometimes give Schtroumpf")
    func transformNonFrenchNounWithInitialCapitalShouldSometimesGiveSchtroumpf() {
        let frenchNoun = "Une Arbalète est ici."
        let expected = "Une Schtroumpf est ici."
        var schtroumpf = false
        for _ in 1...20 {
            if transformer.transform(frenchNoun) == expected {
                schtroumpf = true
                break
            }
        }
        #expect(schtroumpf == true)
    }

    @Test("Transform French plural noun should sometimes give schtroumpfs")
    func transformNonFrenchPluralNounShouldSometimesGiveSchtroumpfs() {
        let frenchNoun = "Les arbalètes sont ici."
        let expected = "Les schtroumpfs sont ici."
        var schtroumpf = false
        for _ in 1...20 {
            if transformer.transform(frenchNoun) == expected {
                schtroumpf = true
                break
            }
        }
        #expect(schtroumpf == true)
    }

    @Test("Transform French être or avoir verbs should never give schtroumpfer")
    func transformFrenchEtreOrAvoirVerbsShouldNeverGiveSchtroumpfer() {
        let frenchSentence = "Il faut être et non pas avoir."
        var badSchtroumpf = false
        for _ in 1...20 {
            let transformed = transformer.transform(frenchSentence)
            if !transformed.contains("être") || !transformed.contains("avoir") {
                badSchtroumpf = true
                break
            }
        }
        #expect(badSchtroumpf == false)
    }

    @Test("Transform French infinitive verb should sometimes give schtroumpfer")
    func transformNonFrenchInfinitiveVerbShouldSometimesGiveSchtroumpfer() {
        let frenchSentence = "Il ne faut pas marcher dans…"
        let expected = "Il ne faut pas schtroumpfer dans…"
        var transformed = false
        for _ in 1...20 {
            if transformer.transform(frenchSentence) == expected {
                transformed = true
                break
            }
        }
        #expect(transformed == true)
    }

    @Test("Transform some French adverb (ending with ement) should sometimes give schtroumpfement")
    func transformSomeFrenchAdverbsShouldSometimesGiveSchtroumpfement() {
        let frenchSentence = "Je marcherai certainement plus vite."
        let expected = "Je marcherai schtroumpfement plus vite."
        var transformed = false
        for _ in 1...20 {
            if transformer.transform(frenchSentence) == expected {
                transformed = true
                break
            }
        }
        #expect(transformed == true)
    }



}
