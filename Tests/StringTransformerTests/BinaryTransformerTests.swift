//
//  BinaryTransformerTests.swift
//  
//
//  Created by Joël Brogniart on 13/07/2021.
//

import Testing
@testable import StringTransformer

@Suite("Binary transformer test")
struct BinaryTransformerTests {
    let transformer = BinaryTransformer()
    let name = String.Transformer.binary
    let purity = true

    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }

    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "abc defg hijkl mnopqr stuvwxy z"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }

    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }

    @Test("Binary transform ab should give 0x6162")
    func binaryTransformAbShouldBeBinary0x6162()  {
        let abString = "ab"
        let abBinaryString = "01100001" + "01100010"
        #expect(transformer.transform(abString) == abBinaryString)
    }
    
    @Test("Binary transform unicode hammer and sickle should give 0xE298AD")
    func binaryTransformHammerAndSickleShouldBeBinary0xE298AD()  {
        let hammerAndSickleString = "☭"
        let hammerAndSickleBinaryString = "11100010" + "10011000" + "10101101"
        #expect(transformer.transform(hammerAndSickleString) == hammerAndSickleBinaryString)
    }
}
