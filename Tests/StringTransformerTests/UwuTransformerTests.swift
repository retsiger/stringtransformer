//
//  UwuTransformerTests.swift
//  
//
//  Created by Joël Brogniart on 21/09/2021.
//

import Testing
@testable import StringTransformer

@Suite("Uwu transformer test")
struct UwuTransformerTests {
    let transformer = UwuTransformer()
    let name = String.Transformer.uwu
    let purity = false

    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }
    
    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "abc defg hijkl mnopqr stuvwxy z"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }

    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }

    @Test("Uwu transformed string should not contains l or L")
    func uwuTransformedStringShouldNotContainsL() {
        let input = "See you later aligator!"
        let transformed = transformer.transform(input)
        var l: Character = "l"
        #expect(!transformed.contains(l))
        l = "L"
        #expect(!transformed.contains(l))
    }
    
    @Test("Uwu transformed string should not contains r or R")
    func uwuTransformedStringShouldNotContainsR() {
        let input = "See you later aligator!"
        let transformed = transformer.transform(input)
        var r: Character = "r"
        #expect(!transformed.contains(r))
        r = "R"
        #expect(!transformed.contains(r))
    }
}
