//
//  GNUTransformerTests.swift
//
//
//  Created by Joël Brogniart on 15/09/2021.
//

import Testing
@testable import StringTransformer

@Suite("GNU transformer test")
struct GNUTransformerTests {
    let transformer = GNUTransformer()
    let name = String.Transformer.gnu
    let purity = true

    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }

    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "abc defg hijkl mnopqr stuvwxy z"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }

    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }

    @Test("Transform words should give GNU/word")
    func gnuTransformWordsShouldGiveGNUSlashWords() {
        let input = "abcd efgh jklm. uu!"
        let expected = "GNU/abcd GNU/efgh GNU/jklm. GNU/uu!"
        #expect(transformer.transform(input) == expected)
    }

    @Test("Transform four words should return height words")
    func gnuTransformFourWordsShouldGiveHeightWords() {
        let input = "abcd bcde cdef defg"
        let transformed = transformer.transform(input)
        var inputWords = [String]()
        input.enumerateSubstrings(in: input.startIndex..<input.endIndex, options: .byWords) { (substring, _, _, _) -> () in
            if substring != nil {
                inputWords.append(substring!)
            }
        }
        var transformedWords = [String]()
        transformed.enumerateSubstrings(in: transformed.startIndex..<transformed.endIndex, options: .byWords) { (substring, _, _, _) -> () in
            if substring != nil {
                transformedWords.append(substring!)
            }
        }
        #expect(inputWords.count == 4)
        #expect(transformedWords.count == 8)
    }

    @Test("Transformed word should not be identical to original")
    func gnuTransformedWordShouldNotIdenticalToOriginal() {
        let inputWord = "abcdefghijklmnopqzrstuvwxyz"
        let transformedWord = transformer.transform(inputWord)
        #expect(inputWord != transformedWord)
    }
}
