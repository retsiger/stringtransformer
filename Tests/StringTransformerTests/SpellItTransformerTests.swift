//
//  SpellItTransformerTests.swift
//  
//
//  Created by Joël Brogniart on 14/09/2021.
//

import Foundation

import Testing
@testable import StringTransformer

@Suite("SpellIt transformer test")
struct SpellItTransformerTests {
    let transformer = SpellItTransformer()
    let name = String.Transformer.spellit
    let purity = true

    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }

    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "abc defg hijkl mnopqr stuvwxy z"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }

    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }


    @Test("SpellIt transform of aB1 string should be ay bee one period (in English)")
    func spellItTransform_AB1Period_ShouldReturnAyBeeOnePeriod() {
        let aB1PeriodString = "aB1."
        var expectedResult: String
        let locale = Locale.preferredLanguages[0] as String
        switch locale {
            case "en":
                expectedResult = "ay bee one period"
            case "fr-FR":
                expectedResult = "a bé un point"
        default:
            expectedResult = "unknown locale \"\(String(describing: locale))\""
        }
        #expect(transformer.transform(aB1PeriodString) == expectedResult, "locale \"\(locale)\"")
    }
    
    @Test("SpellIt transform string with unknown characters should give the same string")
    func spellItTransformStringWithUnknowCharactersShouldReturnTheSameString() {
        let stringWithUnknownCharacters = "éçàè §"
        let expectedResult = stringWithUnknownCharacters.map({String($0)}).joined(separator: " ")
        #expect(transformer.transform(stringWithUnknownCharacters) == expectedResult)
    }
}
