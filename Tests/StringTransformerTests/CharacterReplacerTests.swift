//
//  CharacterReplacerTests.swift
//
//
//  Created by Joël Brogniart on 15/03/2024.
//

import Testing
@testable import StringTransformer

@Suite("Character replacer tests")
struct CharacterReplacerTests {
    struct TestCharacterReplacer: CharacterReplacer {}
        
    // Tests for characterToCharacterReplace(_ text: String, _ replacementDictionary: [Character: Character])
    @Suite("characterToCharacterReplace(_ text: String, _ replacementDictionary: [Character: Character]")
    struct CharacterToCharacterReplaceWithCharacterToCharacterTableTests {
        let testCharacterReplacer = TestCharacterReplacer()

        @Test("characterToCharacterReplace an empty string with a CharToCharDict should return an empty string")
        func charToCharReplaceEmptyStringWithCharToCharDictShouldBeEmptyString() {
            let input = ""
            let charToCharDict: [Character: Character] = ["a":"A", "b":"B", "c":"C"]
            let result = testCharacterReplacer.characterToCharacterReplace(input, charToCharDict)
            #expect(input == result)
        }
        
        @Test("characterToCharacterReplace a string with an empty CharToCharDict should not change the string")
        func charToCharReplaceStringWithEmptyCharToCharDictShouldNotBeChanged() {
            let input = "abc"
            let charToCharDict: [Character: Character] = [:]
            let result = testCharacterReplacer.characterToCharacterReplace(input, charToCharDict)
            #expect(input == result)
        }
        
        @Test("characterToCharacterReplace a string with characters not in the CharToCharDict should not change the string")
        func charToCharReplaceStringWithCharNotInDictWithCharToCharDictShouldNotBeChanged() {
            let input = "xyz"
            let charToCharDict: [Character: Character] = ["a":"X", "b":"Y", "c":"Z"]
            let result = testCharacterReplacer.characterToCharacterReplace(input, charToCharDict)
            #expect(input == result)
        }
        
        @Test("characterToCharacterReplace a string with a CharToCharDict should correctly replace all characters")
        func charToCharReplaceStringWithCharToCharDictShouldCorrectlyReplaceAll() {
            let input = "abc bca cab"
            let expectedResult = "ABC BCA CAB"
            let charToCharDict: [Character: Character] = ["a":"A", "b":"B", "c":"C"]
            let result = testCharacterReplacer.characterToCharacterReplace(input, charToCharDict)
            #expect(expectedResult == result)
        }
        
        @Test("characterToCharacterReplace a string with a CharToCharDict should have the same number of characters")
        func charToCharReplaceStringWithCharToCharDictShouldHaveSameNumberOfCharacters() {
            let input = "abc bca cab 🙈 🙉 🙊 é"
            let charToCharDict: [Character: Character] = ["a":"A", "b":"B", "c":"C"]
            let result = testCharacterReplacer.characterToCharacterReplace(input, charToCharDict)
            #expect(input.count == result.count)
        }
        
        @Test("characterToCharacterReplace a string with a CharToCharDict should not be cumulative")
        func charToCharReplaceStringWithCharToCharDictShouldNotBeCumulative() {
            let input = "abc bca cab"
            let expectedResult = "bca cab abc"
            let charToCharDict: [Character: Character] = ["a":"b", "b":"c", "c":"a"]
            let result = testCharacterReplacer.characterToCharacterReplace(input, charToCharDict)
            #expect(expectedResult == result)
        }
    }

    // Tests for characterToCharacterReplace(_ text: String, _ replacementDictionary: [Character: String])
    @Suite("characterToCharacterReplace(_ text: String, _ replacementDictionary: [Character: String]")
    struct CharacterToCharacterReplaceWithCharacterToStringTableTests {
        let testCharacterReplacer = TestCharacterReplacer()

        
        @Test("characterToCharacterReplace an empty string with a CharToStringDict should return an empty string")
        func charToCharReplaceEmptyStringWithCharToStringrDictShouldBeEmptyString() {
            let input = ""
            let charToCharDict: [Character: String] = ["a":"VWX", "b":"WXY", "c":"XYZ"]
            let result = testCharacterReplacer.characterToCharacterReplace(input, charToCharDict)
            #expect(input == result)
        }
        
        @Test("characterToCharacterReplace a string with an empty CharToStringDict should not change the string")
        func charToCharReplaceStringWithEmptyCharToStringDictShouldNotBeChanged() {
            let input = "abc"
            let charToCharDict: [Character: String] = [:]
            let result = testCharacterReplacer.characterToCharacterReplace(input, charToCharDict)
            #expect(input == result)
        }
        
        @Test("characterToCharacterReplace a string with characters not in the CharToStringDict should not change the string")
        func charToCharReplaceStringWithCharNotInDictWithCharToStringDictShouldNotBeChanged() {
            let input = "xyz"
            let charToCharDict: [Character: String] = ["a":"VWX", "b":"WXY", "c":"XYZ"]
            let result = testCharacterReplacer.characterToCharacterReplace(input, charToCharDict)
            #expect(input == result)
        }
        
        @Test("characterToCharacterReplace a string with a CharToStringDict should correctly replace all characters")
        func charToCharReplaceStringWithCharToStringDictShouldCorrectlyReplaceAll() {
            let input = "abc bca cab"
            let expectedResult = "ABC BCA CAB"
            let charToCharDict: [Character: String] = ["a":"A", "b":"B", "c":"C"]
            let result = testCharacterReplacer.characterToCharacterReplace(input, charToCharDict)
            #expect(expectedResult == result)
        }
        
        @Test("characterToCharacterReplace a string with a CharToStringDict should have the same number of characters")
        func charToCharReplaceStringWithCharToStringDictShouldHaveSameNumberOfCharacters() {
            let input = "abc bca cab 🙈 🙉 🙊 é"
            let charToCharDict: [Character: String] = ["a":"A", "b":"B", "c":"C", "🙈":"xyz"]
            let result = testCharacterReplacer.characterToCharacterReplace(input, charToCharDict)
            #expect(input.count == result.count)
        }
        
        @Test("characterToCharacterReplace a string with a CharToStringDict should not be cumulative")
        func charToCharReplaceStringWithCharToStringDictShouldNotBeCumulative() {
            let input = "abc bca cab"
            let expectedResult = "bca cab abc"
            let charToCharDict: [Character: String] = ["a":"b", "b":"c", "c":"a"]
            let result = testCharacterReplacer.characterToCharacterReplace(input, charToCharDict)
            #expect(expectedResult == result)
        }
        
        @Test("characterToCharacterReplace a string with a lot of same character with CharToStringDict should be different")
        func charToCharReplaceStringWithALotOfSameCharWithCharToStringDictOutputShouldBeChanged() {
            let input = "aaaaaaaaaaa"
            let charToStringDict: [Character: String] = ["a": "aªąɐɑɒᵃᵄᵅᶏḁạ𝒶𝔞𝕒"]
            let result = testCharacterReplacer.characterToCharacterReplace(input, charToStringDict)
            #expect(input != result)
        }
    }

    // Tests for characterToCharacterWeighedReplace(_ text: String, _ replacementDictionary: [Character: String])
    @Suite("characterToCharacterWeighedReplace(_ text: String, _ replacementDictionary: [Character: String])")
    struct CharacterToCharacterWeighedReplaceWithCharacterToStringTableTests {
        let testCharacterReplacer = TestCharacterReplacer()

        
        @Test("characterToCharacterWeighedReplace empty string with CharToStringDict should be empty string")
        func charToCharWeightedReplaceEmptyStringWithCharToStringDictShouldBeEmptyString() {
            let input = ""
            let charToStringDict: [Character: String] = ["a":"VWX", "b":"WXY", "c":"XYZ"]
            let result = testCharacterReplacer.characterToCharacterWeighedReplace(input, charToStringDict, weight: 5)
            #expect(input == result)
        }
        
        @Test("characterToCharacterWeighedReplace string with empty CharToStringDict should not be changed")
        func charToCharWeighedReplaceStringWithEmptyCharToStringDictShouldNotBeChanged() {
            let input = "abc"
            let charToCharDict: [Character: String] = [:]
            let result = testCharacterReplacer.characterToCharacterWeighedReplace(input, charToCharDict, weight: 5)
            #expect(input == result)
        }
        
        @Test("characterToCharacterWeighedReplace string with characters not in CharToStringDict should not be changed")
        func charToCharWeightedReplaceStringWithCharNotInDictWithCharToStringDictShouldNotBeChanged() {
            let input = "xyz"
            let charToStringDict: [Character: String] = ["a":"VWX", "b":"WXY", "c":"XYZ"]
            let result = testCharacterReplacer.characterToCharacterWeighedReplace(input, charToStringDict, weight: 5)
            #expect(input == result)
        }
    }

    // Tests for characterToStringReplace(_ text: String, _ replacementDictionary: [Character: String])
    @Suite("characterToStringReplace(_ text: String, _ replacementDictionary: [Character: String])")
    struct CharacterToStringdReplaceWithCharacterToStringTableTests {
        let testCharacterReplacer = TestCharacterReplacer()

        
        @Test("characterToStringReplace empty string with CharToCharDict should be empty string")
        func charToStringReplaceEmptyStringWithCharToStringDictShouldBeEmptyString() {
            let input = ""
            let charToStringDict: [Character: String] = ["a":"VWX", "b":"WXY", "c":"XYZ"]
            let result = testCharacterReplacer.characterToStringReplace(input, charToStringDict)
            #expect(input == result)
        }
        
        @Test("characterToStringReplace string with empty CharToStringDict should not be changed")
        func charToStringReplaceStringWithEmptyCharToStringDictShouldNotBeChanged() {
            let input = "abc"
            let charToCharDict: [Character: String] = [:]
            let result = testCharacterReplacer.characterToStringReplace(input, charToCharDict)
            #expect(input == result)
        }
        
        @Test("characterToStringReplace string with characters not in CharToStringDict should not be changed")
        func charToStringReplaceStringWithCharNotInDictWithCharToStringDictShouldNotBeChanged() {
            let input = "xyz"
            let charToStringDict: [Character: String] = ["a":"VWX", "b":"WXY", "c":"XYZ"]
            let result = testCharacterReplacer.characterToStringReplace(input, charToStringDict)
            #expect(input == result)
        }
    }

    // Tests for characterToStringSpacedReplace(_ text: String, _ replacementDictionary: [Character: String])
    @Suite("characterToStringSpacedReplace(_ text: String, _ replacementDictionary: [Character: String])")
    struct CharacterToStringdSpacedReplaceWithCharacterToStringTableTests {
        let testCharacterReplacer = TestCharacterReplacer()

        @Test("characterToStringSpacedReplace empty string with CharToStringDict should be empty")
        func charToStringSpacedReplaceEmptyStringWithCharToStringDictShouldBeEmptyString() {
            let input = ""
            let charToStringDict: [Character: String] = ["a":"VWX", "b":"WXY", "c":"XYZ"]
            let result = testCharacterReplacer.characterToStringSpacedReplace(input, charToStringDict)
            #expect(input == result)
        }
        
        @Test("characterToStringSpacedReplace string with empty CharToStringDict should not be changed")
        func charToStringSpacedReplaceStringWithEmptyCharToStringDictShouldNotBeChanged() {
            let input = "abc"
            let charToCharDict: [Character: String] = [:]
            let result = testCharacterReplacer.characterToStringSpacedReplace(input, charToCharDict)
            #expect(input == result)
        }
        
        @Test("characterToStringSpacedReplace string with CharToStringDict should be spaced")
        func charToStringSpacedReplaceStringWithCharToStringDictShouldBeSpaced() {
            let input = "abc"
            let expectedResult = "A B C"
            let charToStringDict: [Character: String] = ["a":"A", "b":"B", "c":"C"]
            let result = testCharacterReplacer.characterToStringSpacedReplace(input, charToStringDict)
            #expect(expectedResult == result)
        }
        
        @Test("characterToStringSpacedReplace string with characters not in CharToStringDict should be spaced")
        func charToStringSpacedReplace_StringWithCharNotInDict_with_CharToStringDict_ShouldAddSpace() {
            let input = "xyz"
            let expectedResult = "x y z"
            let charToStringDict: [Character: String] = ["a":"VWX", "b":"WXY", "c":"XYZ"]
            let result = testCharacterReplacer.characterToStringSpacedReplace(input, charToStringDict)
            #expect(expectedResult == result)
        }
    }
    
    // Tests for cleanCharacters(_ text :String)
    @Suite("cleanCharacters(_ text :String)")
    struct CleanCharactersTests {
        let testCharacterReplacer = TestCharacterReplacer()

        
        @Test("cleanCharacters empty string should be empty string")
        func cleanCharactersEmptyStringShouldBeEmptyString() {
            let input = ""
            let result = testCharacterReplacer.cleanCharacters(input)
            #expect(input == result)
        }
        
        @Test("cleanCharacters repeated char should be single character")
        func cleanCharactersRepeatedCharShouldBeSingleChar() {
            let input = "aaaaaaaa"
            let result = testCharacterReplacer.cleanCharacters(input)
            #expect("a" == result)
        }
        
        @Test("cleanCharacters string should be ordered characters")
        func cleanCharactersStringShouldBeOrderedChars() {
            let input = "cba"
            let result = testCharacterReplacer.cleanCharacters(input)
            #expect("abc" == result)
        }
    }

    // Tests for CleanCharacterToCharacterTable(_ table: [Character: String])
    @Suite("CleanCharacterToCharacterTable(_ table: [Character: String])")
    struct CleanCharacterToCharacterTableTests {
        let testCharacterReplacer = TestCharacterReplacer()

        @Test("CleanCharacterToCharacterTable empty table should be empty table")
        func cleanCharToCharTableEmptyTableShouldBeEmptyTable() {
            let input: [Character: String] = [:]
            let result = testCharacterReplacer.CleanCharacterToCharacterTable(input)
            #expect(input == result)
        }
        
        @Test("CleanCharacterToCharacterTable repeated char should be single char")
        func cleanCharToCharTableRepeatedCharElementShouldBeSingleCharElement() {
            let input: [Character: String] = ["a":"aaa"]
            let expectedResult: [Character: String] = ["a":"a"]
            let result = testCharacterReplacer.CleanCharacterToCharacterTable(input)
            #expect(expectedResult == result)
        }
        
        @Test("CleanCharacterToCharacterTable string element should be ordered characters element")
        func cleanCharToCharTableStringElementShouldBeBeOrderedCharsElement() {
            let input: [Character: String] = ["a":"cba"]
            let expectedResult: [Character: String] = ["a":"abc"]
            let result = testCharacterReplacer.CleanCharacterToCharacterTable(input)
            #expect(expectedResult == result)
        }
    }
}
