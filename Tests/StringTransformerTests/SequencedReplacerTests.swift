//
//  SequencedReplacerTests.swift
//
//
//  Created by Joël Brogniart on 15/09/2021.
//

import Testing
@testable import StringTransformer

@Suite("SequencedReplacer tests")
struct SequencedReplacerTests {
    struct TestSequencedReplacer: SequencedReplacer {}
    
    // Tests for sequencedCharacterToCharacterReplace(_ text: String, _ replacementArray: [CharacterToCharacter])
    @Suite("sequencedCharacterToCharacterReplace")
    struct SequencedCharacterToCharacterReplaceTests {
        let testSequencedReplacer = TestSequencedReplacer()

        @Test("sequencedCharacterToCharacterReplace empty string should return empty string")
        func sequencedCharacterToCharacterReplaceEmptyShouldBeEmpty() {
            let characterToCharacterTable = [CharacterToCharacter]("ab", "AB")
            let emptyString = ""
            #expect(testSequencedReplacer.sequencedCharacterToCharacterReplace(emptyString, characterToCharacterTable) == emptyString)
        }
        
        @Test("sequencedCharacterToCharacterReplace string with empty table should return original string")
        func sequencedCharacterToCharacterReplaceWithEmptyTableShouldReturnOriginal() {
            let characterToCharacterTable: [CharacterToCharacter] = []
            let input = "abc bca cab"
            #expect(testSequencedReplacer.sequencedCharacterToCharacterReplace(input, characterToCharacterTable) == input)
        }
        
        @Test("sequencedCharacterToCharacterReplace should correcxtly replace all characters in table")
        func sequencedCharacterToCharacterReplaceShouldCorrectlyReplaceAllCharactersInTable() {
            let characterToCharacterTable = [CharacterToCharacter]("ab", "AB")
            let input = "abc bca cab"
            let expectedResult = "ABc BcA cAB"
            #expect(testSequencedReplacer.sequencedCharacterToCharacterReplace(input, characterToCharacterTable) == expectedResult)
        }
        
        @Test("sequencedCharacterToCharacterReplace should be cumulative")
        func sequencedCharacterToCharacterReplaceShouldBeCumulative() {
            let characterToCharacterTable = [CharacterToCharacter]("abc", "bcd")
            let input = "abc bca cab"
            let expectedResult = "ddd ddd ddd"
            #expect(testSequencedReplacer.sequencedCharacterToCharacterReplace(input, characterToCharacterTable) == expectedResult)
        }
    }
    
    // Tests for sequencedCharactersToStringReplace()
    @Suite("sequencedCharactersToStringReplace")
    struct SequencedCharacterToStringReplaceTests {
        let testSequencedReplacer = TestSequencedReplacer()

        @Test("sequencedCharactersToStringReplace empty string should return empty string")
        func test_multipleCharactersToStringReplace_Empty_ShouldBeEmpty() {
            let charactersToStringTable = [
                CharactersToString("ab","XX"),
                CharactersToString("c","zzz"),
            ]
            let emptyString = ""
            #expect(testSequencedReplacer.sequencedCharactersToStringReplace(emptyString, charactersToStringTable) == emptyString)
        }
        
        @Test("sequencedCharactersToStringReplace string with empty table should return original string")
        func sequencedCharactersToStringReplaceWithEmptyTableShouldReturnOriginal() {
            let characterToStringTable: [CharactersToString] = []
            let input = "abc bca cab"
            #expect(testSequencedReplacer.sequencedCharactersToStringReplace(input, characterToStringTable) == input)
        }
        
        @Test("sequencedCharactersToStringReplace should correcxtly replace all characters in table")
       func sequencedCharactersToStringReplaceShouldCorrectlyReplaceAll() {
            let charactersToStringTable = [
                CharactersToString("ab","XX"),
                CharactersToString("c","zzz"),
            ]
            let input = "abc bca cab"
            let expectedResult = "XXXXzzz XXzzzXX zzzXXXX"
           #expect(testSequencedReplacer.sequencedCharactersToStringReplace(input, charactersToStringTable) == expectedResult)
        }
        
        @Test("sequencedCharactersToStringReplace should be cumulative")
        func sequencedCharactersToStringReplaceShouldBeCumulative() {
            let charactersToStringTable = [
                CharactersToString("ab","XX"),
                CharactersToString("X","zzz"),
            ]
            let input = "abc bca cab"
            let expectedResult = "zzzzzzzzzzzzc zzzzzzczzzzzz czzzzzzzzzzzz"
            #expect(testSequencedReplacer.sequencedCharactersToStringReplace(input, charactersToStringTable) == expectedResult)
        }
    }
    
    // Tests for sequencedStringToStringReplace()
    @Suite("sequencedStringToStringReplace")
    struct SequencedStringToStringReplaceTests {
        let testSequencedReplacer = TestSequencedReplacer()

        @Test("sequencedStringToStringReplace empty string should return empty string")
        func sequencedStringToStringReplace_Empty_ShouldBeEmpty() {
            let stringToStringTable = [
                StringToString("ab","XX"),
                StringToString("c","zzz"),
            ]
            let emptyString = ""
            #expect(testSequencedReplacer.sequencedStringToStringReplace(emptyString, stringToStringTable) == emptyString)
        }
        
        @Test("sequencedStringToStringReplace string with empty table should return original string")
        func sequencedStringToStringStringReplaceWithEmptyTable_ShouldReturnOriginal() {
            let stringToStringTable: [StringToString] = []
            let input = "abc bca cab"
            #expect(testSequencedReplacer.sequencedStringToStringReplace(input, stringToStringTable) == input)
        }
        
        @Test("sequencedStringToStringReplace should correcxtly replace all characters in table")
        func sequencedStringToStringReplaceShouldCorrectlyReplaceAll() {
            let stringToStringTable = [
                StringToString("ab","XX"),
                StringToString("c","zzz"),
            ]
            let input = "abc bca cab"
            let expectedResult = "XXzzz bzzza zzzXX"
            #expect(testSequencedReplacer.sequencedStringToStringReplace(input, stringToStringTable) == expectedResult)
        }
        
        @Test("sequencedStringToStringReplace should be cumulative")
        func sequencedStringToStringReplaceShouldBeCumulative() {
            let stringToStringTable = [
                StringToString("ab","XX"),
                StringToString("XX","ab"),
            ]
            let input = "abc bca cab"
            let expectedResult = input
            #expect(testSequencedReplacer.sequencedStringToStringReplace(input, stringToStringTable) == expectedResult)
        }
    }
    
    // Tests for sequencedRegexToStringReplace()
    @Suite("sequencedRegexToStringReplace")
    struct SequencedRegexToStringReplaceTests {
        let testSequencedReplacer = TestSequencedReplacer()

        @Test("sequencedRegexToStringReplace empty string should return empty string")
        func test_multipleRegexToStringReplace_mpty_ShouldBeEmpty() {
            let regexToStringTable = [
                RegexToString("ab","XX"),
                RegexToString("c","zzz"),
            ]
            let emptyString = ""
            #expect(testSequencedReplacer.sequencedRegexToStringReplace(emptyString, regexToStringTable) == emptyString)
        }
        
        @Test("sequencedRegexToStringReplace string with empty table should return original string")
        func sequencedRegexToStringReplaceWithEmptyTableShouldReturnOriginal() {
            let regexToStringTable: [RegexToString] = []
            let input = "abc bca cab"
            #expect(testSequencedReplacer.sequencedRegexToStringReplace(input, regexToStringTable) == input)
        }
        
        @Test("sequencedRegexToStringReplace should correcxtly replace all")
        func sequencedRegexToStringReplaceShouldCorrectlyReplaceAll() {
            let regexToStringTable = [
                RegexToString("ab","XX"),
                RegexToString("c","zzz"),
            ]
            let input = "abc bca cab"
            let expectedResult = "XXzzz bzzza zzzXX"
            #expect(testSequencedReplacer.sequencedRegexToStringReplace(input, regexToStringTable) == expectedResult)
        }
        
        @Test("sequencedRegexToStringReplace should be cumulative")
        func sequencedRegexToStringReplaceShouldBeCumulative() {
            let regexToStringTable = [
                RegexToString("ab","XX"),
                RegexToString("XX","ab"),
            ]
            let input = "abc bca cab"
            let expectedResult = input
            #expect(testSequencedReplacer.sequencedRegexToStringReplace(input, regexToStringTable) == expectedResult)
        }
        
        @Test("sequencedRegexToStringReplace with word boundaries in table should be correct")
        func sequencedRegexToStringReplaceWithWordBoundaryInTableShouldBeCorrect() {
            let regexToStringTable = [
                RegexToString("\\bab\\b","X")
            ]
            let input = "ab ab, abc aaba bca cab ab. ab"
            let expectedResult = "X X, abc aaba bca cab X. X"
            #expect(testSequencedReplacer.sequencedRegexToStringReplace(input, regexToStringTable) == expectedResult)
        }
        
        @Test("sequencedRegexToStringReplace with captures in table should be correct")
        func sequencedRegexToStringReplaceWithCapturesInTableShouldBeCorrect() {
            let regexToStringTable = [
                RegexToString("(a)(b)","$2$1")
            ]
            let input = "ab ab, abc aaba bca cab ab. ab"
            let expectedResult = "ba ba, bac abaa bca cba ba. ba"
            #expect(testSequencedReplacer.sequencedRegexToStringReplace(input, regexToStringTable) == expectedResult)
        }
    }
}
