//
//  CircledTransformerTests.swift
//
//
//  Created by Joël Brogniart on 13/03/2024.
//

import Testing
@testable import StringTransformer

@Suite("Circled transformer test")
struct CircledTransformerTests {
    let transformer = CircledTransformer()
    let name = String.Transformer.circled
    let purity = true

    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }

    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "abc defg hijkl mnopqr stuvwxy z"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }


    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }

    @Test("Transform alphabet string should return circled alphabet string")
    func circledTransformAlphabetShouldBeCircledAlphabet() {
        let alphabet = "abcdefghijklmnopqrstuvwxyz"
        let circledAlphabet = "ⓐⓑⓒⓓⓔⓕⓖⓗⓘⓙⓚⓛⓜⓝⓞⓟⓠⓡⓢⓣⓤⓥⓦⓧⓨⓩ"
        #expect(transformer.transform(alphabet) == circledAlphabet)
    }
    
    @Test("Transform numbers string should return circled numbers string")
    func circledTransformNumbersShouldBeCircledNumbers() {
        let numbers = "0123456789"
        let circledNumbers = "⓪①②③④⑤⑥⑦⑧⑨"
        #expect(transformer.transform(numbers) == circledNumbers)
    }
    
    @Test("Transform characters without circled correspondance should return unchanged")
    func cCircledTransformWithoutCircledCorrespondanceShouldReturnInchanged() {
        let withoutCircledCorrespondance = "ɐɑɒɓɔɕɖɗɘəɚɛɜɝɞɟ"
        #expect(transformer.transform(withoutCircledCorrespondance) == withoutCircledCorrespondance)
    }
}
