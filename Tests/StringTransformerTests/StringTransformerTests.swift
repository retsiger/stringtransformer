//
//  StringTransformerTests.swift
//
//
//  Created by Joël Brogniart on 13/07/2021.
//
import Testing
@testable import StringTransformer

@Suite("String transformers test")
struct StringTransformerTests {
    
    @Test("String transformers should not be empty")
    func stringTransformersShouldNotBeEmpty() {
        #expect(String.transformers.count != 0)
    }
    
    @Test("String transformers count should be equal to transformations count")
    func stringTransformersCountShouldBeEqualToTransformationsCount() {
        #expect(String.transformers.count == String.availableTransformers.count)
    }
    
    @Test("All transformations should have corresponding transformer")
    func stringAllTransformationsShouldHaveCorrespondingTransformer() {
        for transformer in String.transformers.allCases() {
            #expect(String.availableTransformers.contains(transformer) == true, "There is no \(transformer) transformer.")
        }
    }
    
    @Test("String description for known transformations should not be empty")
    func stringDescriptionForKnownTransformationsShouldNotBeEmpty() {
        for transformer in String.availableTransformers {
            #expect(String.description(for: transformer) != "", "Description for transformation \(transformer) should not be empty.")
        }
    }
    
    @Test("String transformation of empty string should return an empty string")
    func stringTransformationOfEmptyStringShouldBeEmptyString() {
        let emptyString = ""
        for transformer in String.availableTransformers {
            let result = emptyString.transform(with: transformer)
            #expect(result == emptyString, "Transformation of empty string by \(transformer) is not empty string: \(result).")
        }
    }
    
    @Test("Pure transformation should always return the same result")
    func pureTransformationShouldAllwayReturnTheSameResult() {
        let input = "The 012345 and abcdef for ABCDEF"
        for transformer in String.availableTransformers {
            if String.isPure(transformer) == true {
                let result = input.transform(with: transformer)
                var isDifferent = false
                for _ in 1...20 {
                    if input.transform(with: transformer) != result {
                        isDifferent = true
                        break
                    }
                }
                #expect(!isDifferent, "Pure transformer \(transformer) returned different values for the same input.")
            }
        }
    }
    
    @Test("Non pure transformation should not always return the same result")
    func nonPureTransformationShouldNotAllwayReturnTheSameResult() {
        let input = "Comment on doit quitter la vie et tous ses maux, c’est vous qui le savez, sublimes animaux !"
        for transformer in String.availableTransformers {
            if String.isPure(transformer) == false {
                let result = input.transform(with: transformer)
                var isDifferent = false
                for _ in 1...20 {
                    if input.transform(with: transformer) != result {
                        isDifferent = true
                        break   
                    }
                }
                #expect(isDifferent, "Pure transformer \(transformer) should not allway return the same value for the same input.")
            }
        }
    }
    
    @Test("Availables transformers should be sorted")
    func availableTransformersShouldBeSorted() {
        var min = String.Transformer.anonymous
        for transformer in String.availableTransformers {
            #expect(transformer >= min, "Transformer \(transformer) (\(transformer.rawValue)) should be greater than \(min) (\(min.rawValue)).")
            min = transformer
        }
    }
}
