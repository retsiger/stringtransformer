//
//  Rot13TransformerTests.swift
//  
//
//  Created by Joël Brogniart on 21/07/2021.
//

import Testing
@testable import StringTransformer

@Suite("Rot 13 transformer test")
struct Rot13TransformerTests {
    let transformer = Rot13Transformer()
    let name = String.Transformer.rot13
    let purity = true

    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }

    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "abc defg hijkl mnopqr stuvwxy z"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }

    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }

    @Test("Rot13 transformed alphabet should be rotated alphabet")
    func rot13TransformedAlphabetShouldBeRotatedAlphabet() {
        let alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        let rotatedAlphabet = "nopqrstuvwxyzabcdefghijklmNOPQRSTUVWXYZABCDEFGHIJKLM"
        #expect(transformer.transform(alphabet) == rotatedAlphabet)
    }
    
    @Test("Rot13 transform twice should return original")
    func rot13TransformTwiceShouldReturnToOriginal() {
        let original = "abc ABC ☭!"
        let firstTransformation = transformer.transform(original)
        #expect(transformer.transform(firstTransformation) == original)
    }
}
