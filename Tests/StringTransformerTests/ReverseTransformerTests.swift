//
//  ReverseTransformerTests.swift
//  
//
//  Created by Joël Brogniart on 08/09/2021.
//

import Testing
@testable import StringTransformer

@Suite("Reverse transformer test")
struct ReverseTransformerTests {
    let transformer = ReverseTransformer()
    let name = String.Transformer.reverse
    let purity = true

    @Test("Transformer.name is correct")
    func transformerNameIsCorrect() {
        #expect(transformer.name == name)
    }
    
    @Test("Verify transformer purity")
    func verifyTransformertPurity() {
        #expect(transformer.isPure() == purity)
    }

    @Test("Test transformer purity")
    func testTransformerPurity() {
        let input = "abc defg hijkl mnopqr stuvwxy z"
        let result = transformer.transform(input)
        var isDifferent = false
        if transformer.isPure() {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                    break
                }
            }
        } else {
            for _ in 1...20 {
                if transformer.transform(input) != result {
                    isDifferent = true
                }
            }
        }
        #expect(isDifferent != transformer.isPure())
    }

    @Test("Transform empty string should return empty string")
    func transformEmptyShouldReturnEmpty() {
        let emptyString = ""
        #expect(transformer.transform(emptyString) == emptyString)
    }

    @Test("Reverse transform string should return reversed string")
    func reverseTransformedStringShouldBeReversed() {
        let input = "Text"
        let expected = String(input.reversed())
        #expect(transformer.transform(input) == expected)
    }
}
