// swift-tools-version: 6.0
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "StringTransformer",
    defaultLocalization: "en",
    platforms: [
        .macOS(.v13),
        .iOS(.v17)
    ],
    products: [
        .library(
            name: "StringTransformer",
            targets: ["StringTransformer"])
    ],
    dependencies: [
    ],
    targets: [
        .target(
            name: "StringTransformer",
            dependencies: []),
        .testTarget(
            name: "StringTransformerTests",
            dependencies: ["StringTransformer"])
    ]
)
