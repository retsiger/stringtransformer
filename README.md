# StringTransformer

Generate a string by applying a transformation to a string.

## Overview

StringTransformer is a package that extend the `String` structure to add a set of transformers to the structure. Each corresponding transformer could be called for a string. The transformer returns a modified version of the string.

## Why

After migrating from freenode to libera.chat, I played with the Limneria bot and found the text filter plugin funny. I thought it would be a fun game to try a Swift port of these filters and play with regular expressions. More filters where added later.

Limnoria code could be found at [Github](https://github.com/ProgVal/Limnoria).

Limnoria filters could be tested on chanel [#limnoria-bots](https://web.libera.chat/#limnoria-bots).
To obtain a list of filters type `limnoria list filter`.
To test a filter type `limnoria filter rot13 Some text to transform.`

## Usage

### String.availableTransformers: [String.Transformer]

Getting the list of transformers.
```swift
import StringTransformer

let transformerList = String.availableTransformers
```

### String.description(for transformer: String.Transformer)

Return the description of a transformer.
     
```swift
import StringTransformer

let rot13TransformerDescription = String.description(for: .rot13)
```

### String.isPure(_ transformer: String.Transformer)

Check if a transformer is pure. A pure transformer is a transformer that always returns the same result for a given input. Calling a non pure transformer multiple times for the same string could return different results.

```swift
import StringTransformer

let isUnuidAPureTransformer = String.isPure(.uniud)
// Should be true
let isJeffKAPureTransformer = String.isPure(.jeffk)
// Should be false
```

### public func transform(with transformer: String.Transformer) -> String

Use a transformer to create a new string from a string.

```swift
import StringTransformer

let sentence = "There are no strangers, only friends you have not met yet."
let jeffkSentence = sentence.transform(with: .jeffk)
```


## Transformers

| Transformer        | Description of the result of the transformation                      |
|:-------------------|:---------------------------------------------------------------------|
| `.anonymous`       | Characters remplaced with graphically similar characters.            |
| `.anonymouslight`  | Some characters remplaced with graphically similar characters.       |
| `.aol`             | Text as if an AOL user had said it.                                  |
| `.binary`          | The binary representation of UTF8 encoded text.                      |
| `.caps`            | Text with all letters capitalized                                    |
| `.capwords`        | Text with the first letter of words capitalized                      |
| `.circled`         | Characters remplaced with circled characters.                        |
| `.gnu`             | Text as GNU/RMS would say it.                                        |
| `.hebrew`          | Text with vowels removed.                                            |
| `.hexlify`         | Hexadecimal string value of each byte of UTF8 encoded text           |
| `.jeffk`           | Text as if JeffK had said it himself!                                |
| `.leet`            | The l33tspeak version of text.                                       |
| `.mixedcase`       | Randomly change the case of each letter.                             |
| `.morse`           | Morse code equivalent of text.                                       |
| `.profanity`       | Return a text where bad words are masked by asterisks.               |
|                    | Based on leo-profanity https://github.com/jojoee/leo-profanity.      |
| `.reverse`         | Reverse version of text, first letter become last…                   |
| `.rot13`           | Rotates text 13 characters to the right in the alphabet (only a-z).  |
| `.schtroumpf`      | Text as said by a smurf (French only).                               |
| `.scramble`        | Scramble internal letters of words.                                  |
| `.shrink`          | Shring words longer than 5 ("internationalization" -> "i18n").       |
| `.spellit`         | Phonetically spelled out versio of text.                             |
| `.squish`          | Remove all spaces of text.                                           |
| `.supa1337`        | K-rad translation of text.                                           |
| `.unbinary`        | Character translation of a binary (0 and 1) text.                    |
| `.undup`           | Consecutive duplicated characters removed.                           |
| `.unhexlify`       | Character translation of an hexadecimal (0-9a-f) text.               |
| `.uniud`           | Characters of text are rotated 180°.                                 |
| `.unmorse`         | Text equivalent of a morse string.                                   |
| `.uwu`             | Text in uwu-speak.                                                   |
| .`vowelrot`        | Text with vowels rotated.                                            |

## Examples

```swift
import StringTransformer

/*
 For each transformer, print the name of the transformer and the transformed text.
 */
let text = "Is anything better, anything better?"
for transformer in String.availableTransformers {
    print("\(transformer): \(text.transform(with: transformer))")
}

/*
 Some transformers and their reverse transformer.
 */
func printTransformAndReverse(_ text: String, transformer: String.Transformer, reverse: String.Transformer) {
    let transformed = text.transform(with: transformer)
    let reversed = transformed.transform(with: reverse)
    print("\(transformer) \(text): \(transformed) - \(reverse) \(transformed): \(reversed)")
}

printTransformAndReverse("Hello", transformer: .rot13, reverse: .rot13)

printTransformAndReverse("Hello", transformer: .binary, reverse: .unbinary)

printTransformAndReverse("Hello", transformer: .hexlify, reverse: .unhexlify)

printTransformAndReverse("Hello", transformer: .morse, reverse: .unmorse)

printTransformAndReverse("Hello", transformer: .reverse, reverse: .reverse)
```
