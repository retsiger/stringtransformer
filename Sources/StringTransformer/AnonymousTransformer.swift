//
//  AnonymousTransformer.swift
//
//
//  Created by Joël Brogniart on 13/03/2024.
//

import Foundation

struct AnonymousTransformer: Transformer, CharacterReplacer {
    let name = String.Transformer.anonymous
    
    func description() -> String {
        let description = String(localized: "anonymous-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the anonymous transformer.")
        return description
    }
    
    func isPure() -> Bool {
        return false
    }
    
    /**
     Create a text where each character of the original text is replaced by a graphically similar character.
     
     - Parameter text: Text to transform.
     - Returns: The transformed text.
     */
    func transform(_ text: String) -> String {
        return characterToCharacterReplace(text, Self.anonymousTable)
    }
}

struct AnonymousLightTransformer: Transformer, CharacterReplacer {
    let name = String.Transformer.anonymouslight
    
    func description() -> String {
        let description = String(localized: "anonymouslight-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the anonymouslight transformer.")
        return description
    }
    
    func isPure() -> Bool {
        return false
    }
    
    /**
     Create a text where each character of the original text is replaced with a certain probability by a graphically similar character.
     
     - Parameter text: Text to transform.
     - Returns: The transformed text.
     */
    func transform(_ text: String) -> String {
        return characterToCharacterWeighedReplace(text, AnonymousTransformer.anonymousTable, weight: 3)
    }
}

extension AnonymousTransformer {
    /**
     anonymousTable is a Character to Character [Character: String] remplacement table. The strings have been created by browsing the Unicode ranges to find characters that bear a graphic resemblance to the corresponding index character. They were then cleaned up so that each character only appear once in the string.
     */
    fileprivate static let anonymousTable: [Character: String] = [
        "0": "0⁰₀𝟘𝟶",
        "1": "1₁𝟙𝟷",
        "2": "2₂𝟚𝟸",
        "3": "3₃𝟛𝟹",
        "4": "4⁴₄𝟜𝟺",
        "5": "5⁵₅𝟝𝟻",
        "6": "6⁶₆𝟞𝟼",
        "7": "7⁷₇𝟟𝟽",
        "8": "8⁸₈𝟠𝟾",
        "9": "9⁹₉𝟡𝟿",
        "A": "@AĄȺᏌᗄᴀᴬḀẠ∀𐌀𝒜𝔸",
        "a": "aªąɐɑɒᵃᵄᵅᶏḁạ𝒶𝔞𝕒",
        "B": "BßɃʙᏼᛒᛔᴃᴮᴯᵝᵦḂḄḆℬ𐌁𐌜𝔅𝔹",
        "b": "bƀɓЪъѢѣҍᑲᒃᵇᵬᵷᶀḃḅḇ𐌜𝒷𝔟𝕓",
        "C": "(CÇȻᏟᏣ₡₵ℂ𐌂𝒞",
        "c": "c¢©çȼɔ𝒸𝔠𝕔",
        "D": "DĐᴅᴆᴰḊḌḎḐḒⅅ𐌃𝒟𝔇𝔻",
        "d": "dđȡɖɗԀԁԂԃᑯᒄᵈᵭᶁᶑḋḍḏḑḓⅆ𝒹𝔡𝕕",
        "E": "3EĘƎƏƐƩȨɆЄЗЭᕮᴇᴱᴲḘḚẸ€⅀∃∈∋∑ⱻ𐊤𐌄𐌴𝔈𝔼",
        "e": "eęƏȩɇɘəɚɛɜɝзᵉᵊᵋᵌᶒᶓᶔᶕḙḛẹ℮ℯⅇ∊∍⋲⋳⋴𝔢𝕖",
        "F": "FƑḞℲⅎ𐊥𐌅𝔉𝔽",
        "f": "fƒᵮᶂḟ𝒻𝔣𝕗",
        "G": "GƓǤɢʛᎶᏵᏽ᏾ᴳḠ⅁𝒢𝔊𝔾",
        "g": "gǥɠᵍᶃḡℊ𝔤𝕘",
        "H": "HĦʜҔҢңҤҥӇӈӉӊᴴᵸḢḤḦḨḪḫⱧ",
        "h": "hħƕɥɦʰћҕҺһᏂᚳḣḥḩḫẖℎℏⱨ𐌷𝒽𝔥𝕙",
        "I": "I¡Įİɪᴵ𝕀",
        "i": "i¡įıɨᎥᵢḭⅈ𝒾𝔦𝕚",
        "J": "JɈᒍᒎᴊᴶ𝒥𝔍𝕁",
        "j": "jɉʝʲⅉ𝒿𝔧𝕛",
        "K": "KĸƘҚқҞҠҡᛕᴋᴷḰḲḴⱩ𐌊𐌺𝒦𝔎𝕂",
        "k": "kƙʞҟᵏᶄḱḳḵⱪ𝓀𝕜",
        "L": "L£ĿŁȽʟᒻᴸḶḸḺḼℒ⅂ⱠⱢ𐌋𝔏𝕃",
        "l": "lłȴɫɬɭᶅḻḽℓ𝓁𝔩𝕝",
        "M": "MƜШЩӍӎᛖᴍᴹḾṀṂⱮ𐌌𐌼𝔐𝕄",
        "m": "mɯɰɱшщᵐᵯᶆḿṁṃ𝓂𝔪𝕞",
        "N": "NŊƝȠɴπИиᴎᴺᴻṄṆṈṊ₦ℕ∏𐌍𐌽𝒩𝔑",
        "n": "nŋƞȵɲɳᵑᵰᶇṅṇṉṋⁿ𝓃𝔫𝕟",
        "O": "0OØƟƠǪʘᎤᏅᛜᴓᴼỌ∅𐍈𝒪𝔒𝕆",
        "o": "o°ºơǫɵʘᴓᵒọℴ𝔬𝕠",
        "p": "pƥҏᚦᚧᚹᵖᵱᵽṕṗ𝓅𝔭𝕡",
        "P": "PƤǷҎᑭᚦᚧᚹᴘᴾṔṖℙⱣ𐌓𐌛𝒫𝔓",
        "Q": "Q¶Ɋᑫℚ𝒬𝔔",
        "q": "qɋʠ𝓆𝔮𝕢",
        "R": "R®ƦɌɺʀʁҐґᏒᖆᖇᖈᖉᚱᚴᴙᴚᴿṘṚṜṞℛℜℝⱤ𐍂",
        "r": "rŗɹɻɼɽʳҐґᚴᵣᵲᶉṙṛṝṟ𝓇𝔯𝕣",
        "S": "$S§ŞƧᏕṠṢṤṦṨ∫Ȿ𝒮𝔖𝕊",
        "s": "sşƨȿʂˢᵴᶊṡṣṥṧṩ𝓈𝔰𝕤",
        "T": "TŦƬƮȾҬҭᴛᵀṪṬṮṰ𐌕𝒯𝔗𝕋",
        "t": "tţŧƫƭȶʇʈᵗṫṭṯṱẗ𝓉𝔱𝕥",
        "U": "UŲƯɄЦᴜᵁṲṴṶỤ∪𐌵𝒰𝔘𝕌",
        "u": "uµųưʉцџᵘᵤṳṵṷụ𝓊𝔲𝕦",
        "V": "VƲʋṾ√𐌖𝒱𝔙𝕍",
        "v": "vʋʌᵛᵥᶌṽṿⱽ𝓋𝔳𝕧",
        "W": "WʷѠᏔᵂẀẂẄẆẈⱲ𝒲𝔚𝕎",
        "w": "wʍʷѡᏇẁẃẅẇẉẘⱳ𝓌𝕨",
        "X": "XӼӾẊẌ𐌗𝒳𝔛𝕏",
        "x": "xˣӽӿᵡᵪᶍẋẍ𝓍𝕩",
        "Y": "Y¥ƳɎʏẎỴ⅄𐍅𝒴𝔜𝕐",
        "y": "yƴɏʎУᎽᵞᵧẏẙỵỾỿ𝓎𝕪",
        "Z": "ZƵƷȤӠẐẒẔⱫⱿ𝒵",
        "z": "zƶȥɀʐʑʒʓӠӡᴣᵶᶎẑẓẕℨⱬ𐌶𝓏𝔷𝕫"
    ]
}
