//
//  Replacer.swift
//
//
//  Created by Joël Brogniart on 16/09/2021.
//
//  Abstract:
//  Helper protocol that provides functions to replace characters of strings
//
import Foundation

/**
 A protocol  that provides functions that replace characters of a string by other characters or strings.
 */
protocol CharacterReplacer {    
    /**
     Replace characters by other characters
     
     - Parameter text: The string where the replacement occurs.
     - Parameter replacementDictionary: A [Character: Character] dictionary where the keys are the characters to be remplaced and the values the replacement characters.
     - Returns: A string where each character present in indices of the replacement dictionary is remplaced by the corresponding character value.
     */
    func characterToCharacterReplace(_ text: String, _ replacementDictionary: [Character: Character]) -> String
    
    /**
     Replace characters by strings
     
     - Parameter text: The string where the replacement occurs.
     - Parameter replacementDictionary: A [Character: String] dictionary where the keys are the characters to be remplaced and the values the replacement strings.
     - Returns: A string where each character present in indices of the replacement dictionary is remplaced by the corresponding string value.
     */
    func characterToStringReplace(_ text: String, _ replacementDictionary: [Character: String]) -> String
    
    /**
     Replace characters by strings
     
     - Parameter text: The string where the replacement occurs.
     - Parameter replacementDictionary: A [Character: String] dictionary where the keys are the characters to be remplaced and the values the replacement strings.
     - Returns: A string where each character present in indices of the replacement dictionary is remplaced by the corresponding string value. A space is inserted between each replacement.
     */
    func characterToStringSpacedReplace(_ text: String, _ replacementDictionary: [Character: String]) -> String
    
    /**
     Replace characters by characters
     
     - Parameter text: The string where the replacement occurs.
     - Parameter replacementDictionary: A dictionary [Character: String] where the keys are the characters to be remplaced and the values strings of possible remplacement characters.
     - Returns: A string where each character present in indices of the replacement dictionary is remplaced by a random character of the corresponding string value.
     */
    func characterToCharacterReplace(_ text: String, _ replacementDictionary: [Character: String]) -> String

    /**
     Replace characters by characters
     
     - Parameter text: The string where the replacement occurs.
     - Parameter replacementDictionary: A dictionary [Character: String] where the keys are the characters to be remplaced and the values strings of possible remplacement characters.
     - Parameter weight: A number between 1 and 10 indicating the probability that the character will be replaced. The larger the number, the more likely the replacement.
     - Returns: A string where each character present in indices of the replacement dictionary is remplaced by a random character of the corresponding string value.
     */
    func characterToCharacterWeighedReplace(_ text: String, _ replacementDictionary: [Character: String], weight: Int) -> String
}

/**
 Implementation of the Replacer protocol's functions.
 */
extension CharacterReplacer {
    
    func characterToCharacterReplace(_ text: String, _ replacementDictionary: [Character: Character]) -> String {
        if replacementDictionary.isEmpty {
            return text
        }
       let transformed = text.map {(c: Character) -> Character in replacementDictionary[c] ?? c}
        return String(transformed)
    }
    
    func characterToCharacterReplace(_ text: String, _ replacementDictionary: [Character: String]) -> String {
        if replacementDictionary.isEmpty {
            return text
        }
        let transformed = text.map {(c: Character) -> String in
            if let possibleReplacements = replacementDictionary[c] {
                if possibleReplacements.isEmpty {
                    return ""
                } else {
                    return String(possibleReplacements.randomElement()!)
                }
            }
            return String(c)
        }
        return transformed.joined(separator: "")
    }
    
    func characterToCharacterWeighedReplace(_ text: String, _ replacementDictionary: [Character: String], weight: Int) -> String {
        if replacementDictionary.isEmpty {
            return text
        }
        let transformed = text.map {(c: Character) -> String in
            if Int.random(in: 1...10) <= weight, let possibleReplacements = replacementDictionary[c] {
                if possibleReplacements.isEmpty {
                    return ""
                } else {
                    return String(possibleReplacements.randomElement()!)
                }
            }
            return String(c)
        }
        return transformed.joined(separator: "")
    }
    
    func characterToStringReplace(_ text: String, _ replacementDictionary: [Character: String]) -> String {
        if replacementDictionary.isEmpty {
            return text
        }
        let transformed = text.map {(c: Character) -> String in
            replacementDictionary[c] ?? String(c)
        }
        return transformed.joined(separator: "")
    }
    
    func characterToStringSpacedReplace(_ text: String, _ replacementDictionary: [Character: String]) -> String {
        if replacementDictionary.isEmpty {
            return text
        }
        let transformed = text.map {(c: Character) -> String in
            if let substitute = replacementDictionary[c] {
                return substitute
            } else {
                return String(c)
            }
        }
        return transformed.joined(separator: " ")
    }

    /**
     Create a string where each unique character of original string appear only once. The character of the new string are ordered.
     
     - Parameter text: The original string.
     - Returns: A string where each character appears only once and the characters of the string are ordered.
     */
    func cleanCharacters(_ text :String) -> String {
        var uniques :Set<String> = []
        text.forEach {c in
            uniques.insert(String(c))
        }
        let cleaned = uniques.sorted()
        return cleaned.joined(separator: "")
    }

    /**
     Remove duplicate characters of each replacement character list in a Character to Character [Character:String] replacement table.
     
     - Parameter text: A Character to Character [Character: String] replacement table.
     - Returns: A [Character: String] replacement table where each character appears only once in the string values.
     */
    func CleanCharacterToCharacterTable(_ table: [Character: String]) -> [Character: String] {
        var cleanedTable: [Character: String] = [:]
        for(index, characters) in table {
            cleanedTable[index] = cleanCharacters(characters)
        }
        return cleanedTable
    }
}

