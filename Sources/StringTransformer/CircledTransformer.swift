//
//  CircledTransformer.swift
//
//
//  Created by Joël Brogniart on 13/03/2024.
//

import Foundation

struct CircledTransformer: Transformer, CharacterReplacer {
    let name = String.Transformer.circled

    func description() -> String {
        let description = String(localized: "circled-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the circled transformer.")
        return description
    }

    func isPure() -> Bool {
        return true
    }
    
    func transform(_ text: String) -> String {
        return characterToCharacterReplace(text, Self.circledTable)
    }
    
   private static let circledTable: [Character: Character] = Dictionary(uniqueKeysWithValues: zip(Array("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), Array("⓪①②③④⑤⑥⑦⑧⑨ⓐⓑⓒⓓⓔⓕⓖⓗⓘⓙⓚⓛⓜⓝⓞⓟⓠⓡⓢⓣⓤⓥⓦⓧⓨⓩⒶⒷⒸⒹⒺⒻⒼⒽⒾⒿⓀⓁⓂⓃⓄⓅⓆⓇⓈⓉⓊⓋⓌⓍⓎⓏ")))
}
