//
//  SequencedReplacer.swift
//
//
//  Created by Joël Brogniart on 21/07/2021.
//
// Abstract:
// Helper protocol that provides functions to apply sequences of search and replace in strings.
//

import Foundation

/**
 A protocol that provides functions to apply sequences of search and replace in strings.
 */
protocol SequencedReplacer {
    /**
     Apply a sequence of search and replace to a string. Each element of the sequence replace a character by another character.
     
     Replacements are applyed in the order of the elements of `replacementArray` and each replacement is applyed to the result of the previous replacement.
     
     - Parameter text: The string where the replacements are applied.
     - Parameter replacementArray: A array of `CharacterToCharacter`.
     - Returns: The result of the sequence of search and replace in a string.
     */
    func sequencedCharacterToCharacterReplace(_ text: String, _ replacementArray: [CharacterToCharacter]) -> String
    
    /**
     Apply a sequence of search and replace to a string. Each element of the sequence replace some characters by a string.
     
     Replacements are applyed in the order of the elements of `replacementArray` and each replacement is applyed to the result of the previous replacement.
     
     - Parameter text: The string where the replacements are applied.
     - Parameter replacementArray: A array of `CharactersToString`.
     - Returns: The result of the sequence of search and replace in a string.
     */
    func sequencedCharactersToStringReplace(_ text: String, _ replacementArray: [CharactersToString]) -> String
    /**
     Apply a sequence of search and replace to a string. Each element of the sequence replace a string by another string.
     
     Replacements are applyed in the order of the elements of `replacementArray` and each replacement is applyed to the result of the previous replacement.
     
     - Parameter text: The string where the replacements are applied.
     - Parameter replacementArray: A array of `StringToString`.
     - Returns: The result of the sequence of search and replace in a string.
     */
    func sequencedStringToStringReplace(_ text: String, _ replacementArray: [StringToString]) -> String
    /**
     Apply a sequence of search and replace to a string. Each element of the sequence use a regular expression for the search part and replace the result of the regular expression by a string.
     
     Replacements are applyed in the order of the elements of `replacementArray` and each replacement is applyed to the result of the previous replacement.
     
     - Parameter text: The string where the replacements are applied.
     - Parameter replacementArray: A array of `RegexToString`.
     - Returns: The result of the sequence of search and replace in a string.
     */
    func sequencedRegexToStringReplace(_ text: String, _ replacementArray: [RegexToString]) -> String
}

/**
 Implementation of the SequencedReplacer protocol's functions.
 */
extension SequencedReplacer {
    func sequencedCharacterToCharacterReplace(_ text: String, _ replacementArray: [CharacterToCharacter]) -> String {
        var transformed = text
        _ = replacementArray.map{ r in
            transformed = transformed.replacingOccurrences(of: r.search, with: r.replace)
        }
        return transformed
    }

    func sequencedCharactersToStringReplace(_ text: String, _ replacementArray: [CharactersToString]) -> String {
        var transformed = text
        _ = replacementArray.map { r in
            _ = r.search.map { c in
                transformed = transformed.replacingOccurrences(of: String(c), with: r.replace)
            }
        }
        return transformed
    }

    func sequencedStringToStringReplace(_ text: String, _ replacementArray: [StringToString]) -> String {
        var transformed = text
        _ = replacementArray.map{ r in
            transformed = transformed.replacingOccurrences(of: r.search, with: r.replace)
        }
        return transformed
    }

    func sequencedRegexToStringReplace(_ text: String, _ replacementArray: [RegexToString]) -> String {
        var transformed = text
        _ = replacementArray.map{ r in
            transformed = transformed.replacingOccurrences(of: r.search, with: r.replace, options: [.regularExpression])
        }
        return transformed
    }
}

/// A search and replace container to be used when one character shoud be replaced by another character.
struct CharacterToCharacter {
    /// The character to replace.
    let search: String
    /// The replacement character.
    let replace: String
    
    init(_ character: String, _ replaceCharacter: String) {
        self.search = character
        self.replace = replaceCharacter
     }
}

/// A search and replace container to be used when many characters should be replaced by the same string.
struct CharactersToString {
    /// Each character of search will be replaced by the replacement string.
    let search: String
    /// The replacement string.
    let replace: String
    
    init(_ characters: String, _ replaceString: String) {
        self.search = characters
        self.replace = replaceString
    }
}

/// A search and replace container to be used when a string should be replaced by a string.
struct StringToString {
    /// The string to replace.
    let search: String
    /// The replacement string.
    let replace: String
    
    init(_ search: String, _ replace: String) {
        self.search = search
        self.replace = replace
    }
}

/// A search and replace container to be used when the result of a regular expression should be replaced by a string.
struct RegexToString {
    /// The string to search.
    let search: String
    /// The replacement string.
    let replace: String
    
    init(_ regex: String, _ replace: String) {
        self.search = regex
        self.replace = replace
    }
}

/// Array extension to allow the creation of Array of `CharacterToCharacter`from two strings.
extension Array where Element == CharacterToCharacter {
        /**
     Initialize an array of `CharacterToCharacter` structures with two strings.
     
     The first `CharacterToCharacter` of the array is created with the first character of input and the first character of output. The second `CharacterToCharacter` of the array is created with the second character of input and the second character of output. `CharacterToCharacter` creation continue until reaching the end of the shorter of input or output. Other characters from input or output are ignored.
     - Parameter input: String containing all characters to be searched.
     - Parameter output: String containing all corresponding characters remplacement.
     */
    init(_ input: String, _ output: String) {
        var characterToCharacterArray = [CharacterToCharacter]()
        var inputStartIndex = input.startIndex
        var outputStartIndex = output.startIndex
        while inputStartIndex < input.endIndex {
            if outputStartIndex >= output.endIndex {
                break
            }
            let inputEndIndex = input.index(inputStartIndex, offsetBy: 1)
            let outputEndIndex = output.index(outputStartIndex, offsetBy: 1)
            let characterToCharactere = CharacterToCharacter(String(input[inputStartIndex..<outputEndIndex]), String(output[outputStartIndex..<inputEndIndex]))
            characterToCharacterArray.append(characterToCharactere)
            inputStartIndex = inputEndIndex
            outputStartIndex = outputEndIndex
        }
        self = characterToCharacterArray
    }
}
