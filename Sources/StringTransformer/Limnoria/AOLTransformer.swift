//
//  AOL.swift
//
//  Abstract:
//  Returns the text as if an AOL user had said it.
//
//  Created by Joël Brogniart on 08/09/2021.
//

import Foundation

struct AOLTransformer: Transformer, SequencedReplacer {
    let name = String.Transformer.aol

    func description() -> String {
        let description = String(localized: "aol-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the aol transformer.")
        return description
    }
     
    func isPure() -> Bool {
        return false
    }
    
    /**
     Create a text as if an AOL user had said it.
     
     - Parameter text: Text to transform.
     - Returns: The transformed text.
     */
    func transform(_ text: String) -> String {
        if text.isEmpty {
            return text
        }
        var transformed = text
        transformed = sequencedRegexToStringReplace(transformed, Self.regexTransformations)
        let smiley = ["<3", ":)", ":-)", ":D", ":-D"].randomElement()!
        transformed = transformed + String(repeating: smiley, count: 3)
        return transformed
    }
    
    private static let regexTransformations = [
        RegexToString("\\byou\\b", "u"),
        RegexToString("\\bare\\b", "r"),
        RegexToString("\\blove\\b", "<3"),
        RegexToString("\\bluv\\b", "<3"),
        RegexToString("\\btoo\\b", "2"),
        RegexToString("\\bto\\b", "2"),
        RegexToString("\\btwo\\b", "2"),
        RegexToString("fore", "4"),
        RegexToString("\\bfor\\b", "4"),
        RegexToString("be", "b"),
        RegexToString("four", "4"),
        RegexToString("\\btheir\\b", "there"),
        RegexToString(", ", " "),
        RegexToString(",", " "),
        RegexToString("'", ""),
        RegexToString("one", "1")
    ]
}
