//
//  SquishTransformer.swift
//
//  Created by Joël Brogniart on 08/07/2021.
//

import Foundation

struct SquishTransformer: Transformer {
    let name = String.Transformer.squish

    func description() -> String {
        let description = String(localized: "squish-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the squish transformer.")
        return description
    }

    func isPure() -> Bool {
        return true
    }
    
    func transform(_ text: String) -> String {
        return text.replacingOccurrences(of: "\\s*", with: "", options: [.regularExpression])
    }
}
