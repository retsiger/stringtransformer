//
//  JeffKTransformer.swift
//
//  Created by Joël Brogniart on 06/07/2021.
//

import Foundation

struct JeffKTransformer: Transformer, SequencedReplacer {
    let name = String.Transformer.jeffk

    func description() -> String {
        let description = String(localized: "jeffk-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the jeffk transformer.")
        return description
    }
    
    func isPure() -> Bool {
        return false
    }
    
    func transform(_ text: String) -> String {
        if text.isEmpty {
            return text
        }
        var transformed = text
        if Double.random(in: 0..<1) < 0.03 {
            return randomLaugh("NO YUO", probability: 1)
        }
        transformed = sequencedRegexToStringReplace(transformed, Self.allwayRegexTransformations)
        _ = Self.randomRegexTransformations.map {
            transformed = randomlyReplace(transformed, regexToString: $0)
        }
        transformed = quoteOrNothing(transformed)
        transformed = randomExclaim(transformed)
        transformed = randomlyShuffle(transformed, characterSet: "aeiou")
        transformed = randomlyShuffle(transformed, characterSet: "bcdfghkjlmnpqrstvwxyz", probability: 0.4)
        transformed = randomLaugh(transformed)
        if Double.random(in: 0..<1) < 0.4 {
            transformed = transformed.uppercased()
        }
        return transformed
    }
    
    private static let allwayRegexTransformations = [
        RegexToString("er\\b", "ar"),
        RegexToString("\\bthe\\b", "teh"),
        RegexToString("\\byou\\b", "yuo"),
        RegexToString("\\bis\\b", "si"),
        RegexToString("\\blike\\b", "liek"),
        RegexToString("[^e]ing\\b", "eing")
    ]
    
    private static let randomRegexTransformations = [
        RegexToString("i", "ui"),
        RegexToString("le\\b", "al"),
        RegexToString("i", "io"),
        RegexToString("l", "ll"),
        RegexToString("to", "too"),
        RegexToString("that", "taht"),
        RegexToString("[^s]c([ei])", "sci$1"),
        RegexToString("ed\\b", "e"),
        RegexToString("\\band\\b", "adn"),
        RegexToString("\\bhere\\b", "hear"),
        RegexToString("\\bthey're", "their"),
        RegexToString("\\bthere\\b", "they're"),
        RegexToString("\\btheir\\b", "there"),
        RegexToString("that", "taht"),
        RegexToString("[^e]y", "ey")
    ]
    
    private func quoteOrNothing(_ text: String) -> String {
        var ranges: [Range<String.Index>] = []
        let range = NSRange(text.startIndex..., in: text)
        let search = "(\\w)'(\\w)"
        let regex = try! NSRegularExpression(pattern: search, options: [.useUnicodeWordBoundaries])
        let matches = regex.matches(in: text, options: [], range: range)
        matches.forEach { match in
            guard let subrange = Range(match.range(at: 0), in: text) else {
                return
            }
            ranges.append(subrange)
        }
        var transformed = ""
        var last = text.startIndex
        for range in ranges {
            transformed += text[last ..< range.lowerBound]
            let substring = String(text[range])
            let quoteOrNothing = ["$1$2", "$1\"$2"].randomElement()!
            let transformedSubstring = substring.replacingOccurrences(of: search, with: quoteOrNothing, options: [.regularExpression])
            transformed += transformedSubstring
            last = range.upperBound
        }
        transformed += text[last...]
        return transformed
    }
    
    private func randomExclaim(_ text: String) -> String {
        var ranges: [Range<String.Index>] = []
        let range = NSRange(text.startIndex..., in: text)
        let search = "\\.(\\s+|$)"
        let regex = try! NSRegularExpression(pattern: search, options: [.useUnicodeWordBoundaries])
        let matches = regex.matches(in: text, options: [], range: range)
        matches.forEach { match in
            guard let subrange = Range(match.range(at: 0), in: text) else {
                return
            }
            ranges.append(subrange)
        }
        var transformed = ""
        var last = text.startIndex
        for range in ranges {
            transformed += text[last ..< range.lowerBound]
            let substring = String(text[range])
            var randomExclaim = ""
            if Double.random(in: 0..<1) < 0.85 {
                randomExclaim = String(repeating: "!", count: Int.random(in: 1...4) )
            }
            randomExclaim = randomExclaim + "$1"
            let transformedSubstring = substring.replacingOccurrences(of: search, with: randomExclaim, options: [.regularExpression])
            transformed += transformedSubstring
            last = range.upperBound
        }
        transformed += text[last...]
        return transformed
    }
    
    private func randomlyShuffle(_ text: String, characterSet: String, probability: Double = 1.0) -> String {
        var ranges: [Range<String.Index>] = []
        let range = NSRange(text.startIndex..., in: text)
        let search = String(repeating: "([" + characterSet + "])", count: 2)
        let regex = try! NSRegularExpression(pattern: search, options: [.useUnicodeWordBoundaries])
        let matches = regex.matches(in: text, options: [], range: range)
        matches.forEach { match in
            guard let subrange = Range(match.range(at: 0), in: text) else {
                return
            }
            ranges.append(subrange)
        }
        var transformed = ""
        var last = text.startIndex
        for range in ranges {
            transformed += text[last ..< range.lowerBound]
            let substring = String(text[range])
            var randomShuffle = ""
            if Double.random(in: 0..<1) < probability {
                randomShuffle = ["$1$2", "$2$1"].randomElement()!
            } else {
                randomShuffle = "$1$2"
            }
            let transformedSubstring = substring.replacingOccurrences(of: search, with: randomShuffle, options: [.regularExpression])
            transformed += transformedSubstring
            last = range.upperBound
        }
        transformed += text[last...]
        return transformed
    }
    
    private func randomlyReplace(_ text: String, regexToString: RegexToString, probability: Double = 0.5) -> String {
        var ranges: [Range<String.Index>] = []
        let range = NSRange(text.startIndex..., in: text)
        let regex = try! NSRegularExpression(pattern: regexToString.search, options: [])
        let matches = regex.matches(in: text, options: [], range: range)
        matches.forEach { match in
            if Double.random(in: 0..<1) < probability {
                guard let subrange = Range(match.range(at: 0), in: text) else {
                    return
                }
                ranges.append(subrange)
            }
        }
        var transformed = ""
        var last = text.startIndex
        for range in ranges {
            transformed += text[last ..< range.lowerBound]
            let substring = String(text[range])
            
            let transformedSubstring = substring.replacingOccurrences(of: regexToString.search, with: regexToString.replace, options: [.regularExpression])
            transformed += transformedSubstring
            last = range.upperBound
        }
        transformed += text[last...]
        return transformed
    }
    
    private func randomLaugh(_ text: String, probability: Double = 0.3) -> String {
        var transformed = text
        if Double.random(in: 0..<1) < probability {
            var insult = ""
            if Double.random(in: 0..<1) < 0.5 {
                insult = [" fagot1", " fagorts", " jerks", "fagot", " jerk", "dumbshoes", " dumbshoe"].randomElement()!
            }
            transformed = transformed + insult
            let randomLaugh1 = ["ha", "hah", "lol", "l0l", "ahh"].randomElement()!
            let randomLaugh2 = ["ha", "hah", "lol", "l0l", "ahh"].randomElement()!
            let laugh1 = String(repeating: randomLaugh1, count: Int.random(in: 1...randomLaugh1.count))
            let laugh2 = String(repeating: randomLaugh2, count: Int.random(in: 1...randomLaugh2.count))
            var exclaim = ["!", "~", "!~", "~!!~~", "!!~", "~~~!"].randomElement()!
            exclaim += ["!", "~", "!~", "~!!~~", "!!~", "~~~!"].randomElement()!
            if Double.random(in: 0..<1) < 0.5 {
                exclaim += ["!", "~", "!~", "~!!~~", "!!~", "~~~!"].randomElement()!
            }
            transformed += [" ", laugh1, laugh2, insult, exclaim].joined()
        }
        return transformed
    }
}
