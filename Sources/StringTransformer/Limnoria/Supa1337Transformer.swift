//
//  Supa1337Transformer.swift
//  
//
//  Created by Joël Brogniart on 24/07/2021.
//

import Foundation

struct Supa1337Transformer: Transformer, SequencedReplacer {
    let name = String.Transformer.supa1337

    func description() -> String {
        let description = String(localized: "supa1337-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the supa1337 transformer.")
        return description
    }
    
    func isPure() -> Bool {
        return true
    }
    
    func transform(_ text: String) -> String {
        let transformed = sequencedRegexToStringReplace(text, Self.regexTransformations)
        return sequencedCharactersToStringReplace(transformed, Self.supaleetTransformations)
    }
    
    static private let regexTransformations = [
        RegexToString("\\b(?:(?:[yY][o0O][oO0uU])|u)\\b", "j00"),
        RegexToString("fear", "ph33r"),
        RegexToString("[aA][tT][eE]", "8"),
        RegexToString("[aA][tT]", "@"),
        RegexToString("[sS]\\b", "z"),
        RegexToString("x", ""),
    ]

    static private let supaleetTransformations: [CharactersToString] = [
        CharactersToString("xX","><"),
        CharactersToString("kK","|<"),
        CharactersToString("rR","|2"),
        CharactersToString("hH","|-|"),
        CharactersToString("L","|_"),
        CharactersToString("uU","|_|"),
        CharactersToString("O","()"),
        CharactersToString("nN","|\\|"),
        CharactersToString("mM","/\\/\\"),
        CharactersToString("G","6"),
        CharactersToString("sS","$"),
        CharactersToString("i",";"),
        CharactersToString("aA","/-\\"),
        CharactersToString("eE","3"),
        CharactersToString("t","+"),
        CharactersToString("T","7"),
        CharactersToString("l","1"),
        CharactersToString("D","|)"),
        CharactersToString("B","|3"),
        CharactersToString("I","]["),
        CharactersToString("vV","\\/"),
        CharactersToString("wW","\\/\\/"),
        CharactersToString("d","c|"),
        CharactersToString("b","|>"),
        CharactersToString("c","<"),
        CharactersToString("h","|n")
    ]
}
