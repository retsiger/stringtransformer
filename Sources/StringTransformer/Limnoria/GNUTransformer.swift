//
//  GNUTransformer.swift
//  
//
//  Created by Joël Brogniart on 15/09/2021.
//

import Foundation
import NaturalLanguage

struct GNUTransformer: Transformer, SequencedReplacer {
    let name = String.Transformer.gnu

    func description() -> String {
        let description = String(localized: "gnu-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the gnu transformer.")
        return description
    }
    
    func isPure() -> Bool {
        return true
    }
    
    func transform(_ text: String) -> String {
        if #available(macOS 10.14, iOS 12.0, *) {
            var transformed = ""
            var last = text.startIndex
            let tokenizer = NLTokenizer(unit: .word)
            tokenizer.string = text
            tokenizer.enumerateTokens(in: text.startIndex..<text.endIndex) { range, _ in
                transformed += text[last ..< range.lowerBound]
                transformed += "GNU/" + text[range]
                last = range.upperBound
                return true
            }
            transformed += text[last...]
            return transformed
        } else {
            return sequencedRegexToStringReplace(text, [RegexToString("(\\w+)", "GNU/$1")])
        }
    }
}
