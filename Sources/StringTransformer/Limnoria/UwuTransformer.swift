//
//  UwuTransformer.swift
//  
//
//  Created by Joël Brogniart on 21/09/2021.
//

import Foundation

struct UwuTransformer: Transformer, CharacterReplacer {
    let name = String.Transformer.uwu

    func description() -> String {
        let description = String(localized: "uwu-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the uwu transformer.")
        return description
    }
    
    func isPure() -> Bool {
        return false
    }
    
    func transform(_ text: String) -> String {
        if text.isEmpty {
            return text
        }
        let uwuSignature = [String](repeating: "", count: 10) + [" uwu", " UwU", " owo", " OwO"]
        return characterToCharacterReplace(text, Self.characterReplacementTable) + uwuSignature.randomElement()!
    }
    
    static private let characterReplacementTable: [Character: Character] = Dictionary(uniqueKeysWithValues: zip(Array("lrLR"), Array("wwWW")))
}
