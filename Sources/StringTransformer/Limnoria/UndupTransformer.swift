//
//  UndupTransformer.swift
//
//  Created by Joël Brogniart on 08/07/2021.
//

import Foundation

struct UndupTransformer: Transformer {
    let name = String.Transformer.undup

    func description() -> String {
        let description = String(localized: "undup-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the undup transformer.")
        return description
    }

    func isPure() -> Bool {
        return true
    }
    
    func transform(_ text: String) -> String {
        var result = String()
        var lastCharacter: Character?
        text.forEach {c in
                if c != lastCharacter {
                    result.append(c)
                    lastCharacter = c
                }
        }
        return result
    }
}
