//
//  UnhexlifyTransformer.swift
//  
//
//  Created by Joël Brogniart on 20/07/2021.
//

import Foundation

struct UnhexlifyTransformer: Transformer {
    let name = String.Transformer.unhexlify

    func description() -> String {
        let description = String(localized: "unhexlify-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the unhexlify transformer.")
        return description
    }

    func isPure() -> Bool {
        return true
    }
    
    func transform(_ text: String) -> String {
        // keep only characters in 0..9, a..z, A..Z
        let hexadecimal = text.replacingOccurrences(of: "[^a-zA-Z0-9]", with: "", options: [.regularExpression])
        
        // Split input in 2 characters substrings and then convert substrings to int
        var utf8CodeUnits = [UInt8]()
        var startIndex = hexadecimal.startIndex
        while startIndex < hexadecimal.endIndex {
            let endIndex = hexadecimal.index(startIndex, offsetBy: 2, limitedBy: hexadecimal.endIndex) ?? hexadecimal.endIndex
            let byte = hexadecimal[startIndex..<endIndex]
            if let utf8CodeUnit = UInt8(byte, radix: 16) {
                utf8CodeUnits.append(utf8CodeUnit)
            }
            startIndex = endIndex
        }
        return String(decoding: utf8CodeUnits, as: UTF8.self)
    }
}

