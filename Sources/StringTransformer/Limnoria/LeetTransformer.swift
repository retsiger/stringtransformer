//
//  LeetTransformer.swift
//  
//
//  Created by Joël Brogniart on 21/07/2021.
//

import Foundation

struct LeetTransformer: Transformer, SequencedReplacer, CharacterReplacer {
    let name = String.Transformer.leet

    func description() -> String {
        let description = String(localized: "leet-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the anonymouslight transformer.")
        return description
    }
    
    func isPure() -> Bool {
        return true
    }
    
    func transform(_ text: String) -> String {
        let transformed = sequencedRegexToStringReplace(text, Self.regexTransformations)
        return characterToCharacterReplace(transformed, Self.characterTransformations)
    }
    
    private static let characterTransformations: [Character: Character] = Dictionary(uniqueKeysWithValues: zip("oOaAeElBTiIts", "004433187!1+5"))

    private static let regexTransformations = [
        RegexToString("\\b(?:(?:[yY][o0O][oO0uU])|u)\\b", "j00"),
        RegexToString("fear", "ph33r"),
        RegexToString("[aA][tT][eE]", "8"),
        RegexToString("[aA][tT]", "@"),
        RegexToString("[sS]\\b", "z"),
        RegexToString("x", "><")
    ]
}
