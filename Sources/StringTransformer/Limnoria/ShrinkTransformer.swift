//
//  ShrinkTransformer.swift
//  
//
//  Created by Joël Brogniart on 16/09/2021.
//

import Foundation
import NaturalLanguage

struct ShrinkTransformer: Transformer {
    let name = String.Transformer.shrink

    func description() -> String {
        let description = String(localized: "shrink-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the shrink transformer.")
        return description
    }
    
    func isPure() -> Bool {
        return true
    }
    
    func transform(_ text: String) -> String {
        return shrink(text)
    }
    
    private static let minimum = 4
    
    private func shrink(_ text: String, minimum: Int = Self.minimum) -> String {
        var transformed = text
        if #available(macOS 10.14, iOS 12.0, *) {
            var wordRanges: [Range<String.Index>] = []
            let tokenizer = NLTokenizer(unit: .word)
            tokenizer.string = transformed
            tokenizer.enumerateTokens(in: transformed.startIndex..<transformed.endIndex) { range, _ in
                if transformed[range].count > Self.minimum {
                    let subrange = transformed.index(range.lowerBound, offsetBy: 1)..<transformed.index(range.upperBound, offsetBy: -1)
                    wordRanges.append(subrange)
                }
                return true
            }
            wordRanges.reversed().forEach { range in
                let length = String(transformed[range].count)
                transformed.replaceSubrange(range, with: length)
            }
        } else {
            let range = NSRange(transformed.startIndex..., in: transformed)
            let regex = try! NSRegularExpression(pattern: "\\w+", options: [])
            let matches = regex.matches(in: transformed, options: [], range: range)
            matches.reversed().forEach { match in
                guard let subrange = Range(match.range(at: 0), in: transformed) else {
                    return
                }
                let length = transformed[subrange].count
                if length >= minimum {
                    let first = transformed[subrange].first!
                    let last = transformed[subrange].last!
                    let newWord = "\(first)\(String(length - 2))\(last)"
                    transformed.replaceSubrange(subrange, with: newWord)
                }
            }
        }
        return transformed
    }
}
