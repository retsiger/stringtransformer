//
//  ReverseTransformer.swift
//  
//
//  Created by Joël Brogniart on 08/09/2021.
//

import Foundation

struct ReverseTransformer: Transformer {
    let name = String.Transformer.reverse

    func description() -> String {
        let description = String(localized: "reverse-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the reverse transformer.")
        return description
    }

    func isPure() -> Bool {
        return true
    }
    
    func transform(_ text: String) -> String {
        return String(text.reversed())
    }
}
