//
//  SpellItTransformer.swift
//  
//
//  Created by Joël Brogniart on 14/09/2021.
//

import Foundation

struct SpellItTransformer: Transformer, CharacterReplacer {
    let name = String.Transformer.spellit

    func description() -> String {
        let description = String(localized: "spellit-transformer-description",
                                 table: "SpellIt",
                                 bundle: .module,
                                 comment: "Description of the spellit transformer.")
        return description
    }
    
    func isPure() -> Bool {
        return true
    }
    
    func transform(_ text: String) -> String {
        return characterToStringSpacedReplace(text, Self.phoneticCode)
    }

    private static let spellLettersLowercase: [Character: String] = [
        "a" : String(localized: "spell-a", table: "SpellIt", bundle: .module, comment: "The letter a spelled out."),
        "b" : String(localized: "spell-b", table: "SpellIt", bundle: .module, comment: "The letter b spelled out."),
        "c" : String(localized: "spell-c", table: "SpellIt", bundle: .module, comment: "The letter c spelled out."),
        "d" : String(localized: "spell-d", table: "SpellIt", bundle: .module, comment: "The letter d spelled out."),
        "e" : String(localized: "spell-e", table: "SpellIt", bundle: .module, comment: "The letter e spelled out."),
        "f" : String(localized: "spell-f", table: "SpellIt", bundle: .module, comment: "The letter f spelled out."),
        "g" : String(localized: "spell-g", table: "SpellIt", bundle: .module, comment: "The letter g spelled out."),
        "h" : String(localized: "spell-h", table: "SpellIt", bundle: .module, comment: "The letter h spelled out."),
        "i" : String(localized: "spell-i", table: "SpellIt", bundle: .module, comment: "The letter i spelled out."),
        "j" : String(localized: "spell-j", table: "SpellIt", bundle: .module, comment: "The letter j spelled out."),
        "k" : String(localized: "spell-k", table: "SpellIt", bundle: .module, comment: "The letter k spelled out."),
        "l" : String(localized: "spell-l", table: "SpellIt", bundle: .module, comment: "The letter l spelled out."),
        "m" : String(localized: "spell-m", table: "SpellIt", bundle: .module, comment: "The letter m spelled out."),
        "n" : String(localized: "spell-n", table: "SpellIt", bundle: .module, comment: "The letter n spelled out."),
        "o" : String(localized: "spell-o", table: "SpellIt", bundle: .module, comment: "The letter o spelled out."),
        "p" : String(localized: "spell-p", table: "SpellIt", bundle: .module, comment: "The letter p spelled out."),
        "q" : String(localized: "spell-q", table: "SpellIt", bundle: .module, comment: "The letter q spelled out."),
        "r" : String(localized: "spell-r", table: "SpellIt", bundle: .module, comment: "The letter r spelled out."),
        "s" : String(localized: "spell-s", table: "SpellIt", bundle: .module, comment: "The letter s spelled out."),
        "t" : String(localized: "spell-t", table: "SpellIt", bundle: .module, comment: "The letter t spelled out."),
        "u" : String(localized: "spell-u", table: "SpellIt", bundle: .module, comment: "The letter u spelled out."),
        "v" : String(localized: "spell-v", table: "SpellIt", bundle: .module, comment: "The letter v spelled out."),
        "w" : String(localized: "spell-w", table: "SpellIt", bundle: .module, comment: "The letter w spelled out."),
        "x" : String(localized: "spell-x", table: "SpellIt", bundle: .module, comment: "The letter x spelled out."),
        "y" : String(localized: "spell-y", table: "SpellIt", bundle: .module, comment: "The letter y spelled out."),
        "z" : String(localized: "spell-z", table: "SpellIt", bundle: .module, comment: "The letter z spelled out.")
    ]

    private static let _spellLetters = Self.spellLettersLowercase.merging(Dictionary(uniqueKeysWithValues: spellLettersLowercase.map {key, value in (Character(key.uppercased()), value)}), uniquingKeysWith: {(current,_) in current})
    
    private static let punctuation: [Character: String] = [
        "!" : String(localized: "spell-exclam", table: "SpellIt", bundle: .module, comment: "The symbol ! spelled out."),
        "\"": String(localized: "spell-quote", table: "SpellIt", bundle: .module, comment: "The symbol \" spelled out."),
        "#" : String(localized: "spell-pound", table: "SpellIt", bundle: .module, comment: "The symbol # spelled out."),
        "$" : String(localized: "spell-dollar", table: "SpellIt", bundle: .module, comment: "The symbol $ spelled out."),
        "%" : String(localized: "spell-percent", table: "SpellIt", bundle: .module, comment: "The symbol % spelled out."),
        "&" : String(localized: "spell-ampersand", table: "SpellIt", bundle: .module, comment: "The symbol & spelled out."),
        "'" : String(localized: "spell-single-quote", table: "SpellIt", bundle: .module, comment: "The symbol ' spelled out."),
        "(" : String(localized: "spell-paren-l", table: "SpellIt", bundle: .module, comment: "The symbol ( spelled out."),
        ")" : String(localized: "spell-paren-r", table: "SpellIt", bundle: .module, comment: "The symbol ) spelled out."),
        "*" : String(localized: "spell-asterisk", table: "SpellIt", bundle: .module, comment: "The symbol * spelled out."),
        "+" : String(localized: "spell-plus", table: "SpellIt", bundle: .module, comment: "The symbol + spelled out."),
        "," : String(localized: "spell-comma", table: "SpellIt", bundle: .module, comment: "The symbol , spelled out."),
        "-" : String(localized: "spell-minus", table: "SpellIt", bundle: .module, comment: "The symbol - spelled out."),
        "." : String(localized: "spell-period", table: "SpellIt", bundle: .module, comment: "The symbol . spelled out."),
        "/" : String(localized: "spell-slash", table: "SpellIt", bundle: .module, comment: "The symbol / spelled out."),
        ":" : String(localized: "spell-colon", table: "SpellIt", bundle: .module, comment: "The symbol : spelled out."),
        ";" : String(localized: "spell-semicolon", table: "SpellIt", bundle: .module, comment: "The symbol ; spelled out."),
        "<" : String(localized: "spell-less-than", table: "SpellIt", bundle: .module, comment: "The symbol < spelled out."),
        "=" : String(localized: "spell-equals", table: "SpellIt", bundle: .module, comment: "The symbol = spelled out."),
        ">" : String(localized: "spell-greater-than", table: "SpellIt", bundle: .module, comment: "The symbol > spelled out."),
        "?" : String(localized: "spell-question", table: "SpellIt", bundle: .module, comment: "The symbol ? spelled out."),
        "@" : String(localized: "spell-at", table: "SpellIt", bundle: .module, comment: "The symbol @ spelled out."),
        "[" : String(localized: "spell-bracket-l", table: "SpellIt", bundle: .module, comment: "The symbol [ spelled out."),
        "\\" : String(localized: "spell-backslash", table: "SpellIt", bundle: .module, comment: "The symbol \\ spelled out."),
        "]" : String(localized: "spell-bracket-r", table: "SpellIt", bundle: .module, comment: "The symbol ] spelled out."),
        "^" : String(localized: "spell-caret", table: "SpellIt", bundle: .module, comment: "The symbol ^ spelled out."),
        "_" : String(localized: "spell-underscore", table: "SpellIt", bundle: .module, comment: "The symbol _ spelled out."),
        "`" : String(localized: "spell-backstick", table: "SpellIt", bundle: .module, comment: "The symbol ` spelled out."),
        "{" : String(localized: "spell-brace-l", table: "SpellIt", bundle: .module, comment: "The symbol { spelled out."),
        "|" : String(localized: "spell-pipe", table: "SpellIt", bundle: .module, comment: "The symbol | spelled out."),
        "}" : String(localized: "spell-brace-r", table: "SpellIt", bundle: .module, comment: "The symbol } spelled out."),
        "~" : String(localized: "spell-tilde", table: "SpellIt", bundle: .module, comment: "The symbol ~ spelled out.")
    ]
    
    private static let numbers: [Character: String] = [
        "0" : String(localized: "spell-0", table: "SpellIt", bundle: .module, comment: "The number 0 spelled out."),
        "1" : String(localized: "spell-1", table: "SpellIt", bundle: .module, comment: "The number 1 spelled out."),
        "2" : String(localized: "spell-2", table: "SpellIt", bundle: .module, comment: "The number 2 spelled out."),
        "3" : String(localized: "spell-3", table: "SpellIt", bundle: .module, comment: "The number 3 spelled out."),
        "4" : String(localized: "spell-4", table: "SpellIt", bundle: .module, comment: "The number 4 spelled out."),
        "5" : String(localized: "spell-5", table: "SpellIt", bundle: .module, comment: "The number 5 spelled out."),
        "6" : String(localized: "spell-6", table: "SpellIt", bundle: .module, comment: "The number 6 spelled out."),
        "7" : String(localized: "spell-7", table: "SpellIt", bundle: .module, comment: "The number 7 spelled out."),
        "8" : String(localized: "spell-8", table: "SpellIt", bundle: .module, comment: "The number 8 spelled out."),
        "9" : String(localized: "spell-9", table: "SpellIt", bundle: .module, comment: "The number 9 spelled out.")
    ]

    private static let phoneticCode: [Character: String] = Self._spellLetters.merging(punctuation, uniquingKeysWith: {(current,_) in current}).merging(numbers, uniquingKeysWith: {(current,_) in current})
}
