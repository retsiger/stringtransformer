//
//  CapsTransformer.swift
//
//
//  Created by Joël Brogniart on 21/09/2021.
//

import Foundation

struct CapsTransformer: Transformer {
    let name = String.Transformer.caps

    func description() -> String {
        let description = String(localized: "caps-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the caps transformer.")
        return description
    }
    
    func isPure() -> Bool {
        return true
    }
    
    /**
     Create a uppercase version of the original text.
     
     - Parameter text: Text to transform.
     - Returns: The transformed text.
     */
    func transform(_ text: String) -> String {
        if #available(macOS 10.11, iOS 9, *) {
            return text.localizedUppercase
        } else {
            return text.uppercased()
        }
    }
}
