//
//  UnmorseTransformer.swift
//  
//
//  Created by Joël Brogniart on 06/09/2021.
//

import Foundation

struct UnmorseTransformer: Transformer {
    let name = String.Transformer.unmorse

    func description() -> String {
        let description = String(localized: "unmorse-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the unmorse transformer.")
        return description
    }
    
    func isPure() -> Bool {
        return true
    }
    
    func transform(_ text: String) -> String {
        var ranges: [Range<String.Index>] = []
        let text = text.uppercased().replacingOccurrences(of: "_", with: "-")
        let range = NSRange(text.startIndex..., in: text)
        let regex = try! NSRegularExpression(pattern: "([.-]+)", options: [])
        let matches = regex.matches(in: text, options: [], range: range)
        matches.forEach { match in
            guard let subrange = Range(match.range(at: 1), in: text) else {
                return
            }
            ranges.append(subrange)
        }
        var transformed = ""
        var last = text.startIndex
        for range in ranges {
            transformed += text[last ..< range.lowerBound]
            let substring = String(text[range])
            if let character = Self.reverseMorseCode[substring] {
                transformed += character
            }
            last = range.upperBound
        }
        transformed += text[last...]
        transformed = transformed.replacingOccurrences(of: "  ", with: "\0")
        transformed = transformed.replacingOccurrences(of: " ", with: "")
        transformed = transformed.replacingOccurrences(of: "\0", with: " ")
        return transformed
    }
    
    private func _reverseMorseOf(_ morse: String) -> String {
        if let character = Self.reverseMorseCode[morse] {
            return character + " "
        }
        return morse
    }
    
    /*
     It could be best to calculate reverseMorseCode from MorseTransformer.morseCode to
     Simplify maintenance and limit errors.
     In fact it could be problematic as the extended morse code I used in the MorseTransformer
     Class is not reversible. If I "unmorse" a string transformed with MorseTransformer,
     the unmorsed string could be different from the original string.
     In the extended morse code, different characters could have the same morse code. So when
     reverting it couldn't be decided what was the original character. For example, the code
     "..--" could correspond to "ü" or "ŭ".
     In the reverseMorseCode table, when a code could correspond to many characters I only kept
     one character. I kept the one that was the most probable to be seen in a French text.
     And in a dictionnary structure one can't have multiple occurences of the same index.
     */
    
    static private let reverseMorseCode: [String: String] = [
        ".-"        : "A",
        "-..."      : "B",
        "-.-."      : "C",
        "-.."       : "D",
        "."         : "E",
        "..-."      : "F",
        "--."       : "G",
        "...."      : "H",
        ".."        : "I",
        ".---"      : "J",
        "-.-"       : "K",
        ".-.."      : "L",
        "--"        : "M",
        "-."        : "N",
        "---"       : "O",
        ".--."      : "P",
        "--.-"      : "Q",
        ".-."       : "R",
        "..."       : "S",
        "-"         : "T",
        "..-"       : "U",
        "...-"      : "V",
        ".--"       : "W",
        "-..-"      : "X",
        "-.--"      : "Y",
        "--.."      : "Z",
        "-----"     : "0",
        ".----"     : "1",
        "..---"     : "2",
        "...--"     : "3",
        "....-"     : "4",
        "....."     : "5",
        "-...."     : "6",
        "--..."     : "7",
        "---.."     : "8",
        "----."     : "9",
        ".-.-.-"    : ".",
        "--..--"    : ",",
        "..--.."    : "?",
        ".----."    : "'",
        "-.-.--"    : "!",
        "-..-."     : "/",
        "-.--."     : "(",
        "-.--.-"    : ")",
        ".-..."     : "&",
        "---..."    : ":",
        "-.-.-."    : ";",
        "-...-"     : "=",
        ".-.-."     : "+",
        "-....-"    : "-",
        "..--.-"    : "_",
        ".-..-."    : "\"",
        "...-..-"   : "$",
        ".--.-."    : "@",
        ".-.-"      : "æ",
        ".--.-"     : "à",
        "-.-.."     : "ç",
        "..--."     : "ð",
        "..-.."     : "é",
        ".-..-"     : "è",
        "--.-."     : "ĝ",
        ".---."     : "ĵ",
        "--.--"     : "ñ",
        "---."      : "ó",
        "...---..." : "ś",
        "...-."     : "ŝ",
        "----"      : "š",
        ".--.."     : "þ",
        "..--"      : "ü",
        "--..--."   : "ź",
        "--..-"     : "ż"
    ]
}
