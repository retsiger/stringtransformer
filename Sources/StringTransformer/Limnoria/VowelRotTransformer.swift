//
//  VowelRotTransformer.swift
//  
//
//  Created by Joël Brogniart on 21/09/2021.
//

import Foundation

struct VowelRotTransformer: Transformer, CharacterReplacer {
    let name = String.Transformer.vowelrot

    func description() -> String {
        let description = String(localized: "vowelrot-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the vowelrot transformer.")
        return description
    }
    
    func isPure() -> Bool {
        return true
    }
    
func transform(_ text: String) -> String {
        return characterToCharacterReplace(text, Self.vowelRotationTable)
    }
    
    static private let vowelRotationTable: [Character: Character] = Dictionary(uniqueKeysWithValues: zip(Array("aeiouAEIOU"), Array("eiouaEIOUA")))
}
