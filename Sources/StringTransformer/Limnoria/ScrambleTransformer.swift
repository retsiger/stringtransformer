//
//  ScrambleTransformer.swift
//
//
//  Created by Joël Brogniart on 30/08/2021.
//

import Foundation
import NaturalLanguage

struct ScrambleTransformer: Transformer {
    let name = String.Transformer.scramble

    func description() -> String {
        let description = String(localized: "scramble-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the scramble transformer.")
        return description
    }
    
    func isPure() -> Bool {
        return false
    }
    
    func transform(_ text: String) -> String {
        var ranges: [Range<String.Index>] = []
        if #available(macOS 10.14, iOS 12.0, *) {
            let tokenizer = NLTokenizer(unit: .word)
            tokenizer.string = text
            tokenizer.enumerateTokens(in: text.startIndex..<text.endIndex) { range, _ in
                if text[range].count > 3 {
                    let subrange = text.index(range.lowerBound, offsetBy: 1)..<text.index(range.upperBound, offsetBy: -1)
                    ranges.append(subrange)
                }
                return true
            }
        } else {
            let range = NSRange(text.startIndex..., in: text)
            let regex = try! NSRegularExpression(pattern: "\\W?\\w(\\w+)\\w\\W?", options: [])
            let matches = regex.matches(in: text, options: [], range: range)
            matches.forEach { match in
                guard let subrange = Range(match.range(at: 1), in: text) else {
                    return
                }
                ranges.append(subrange)
            }
        }
        var transformed = ""
        var last = text.startIndex
        for range in ranges {
            transformed += text[last ..< range.lowerBound]
            transformed += text[range].shuffled()
            last = range.upperBound
        }
        transformed += text[last...]
        return transformed
    }
}
