//
//  HebrewTransformer.swift
//
//  Created by Joël Brogniart on 30/06/2021.
//

import Foundation

struct HebrewTransformer: Transformer {
    let name = String.Transformer.hebrew

    func description() -> String {
        let description = String(localized: "hebrew-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the hebrew transformer.")
        return description
    }

    func isPure() -> Bool {
        return true
    }
    
    func transform(_ text: String) -> String {
        var result = String()
        for character in text {
            let s = String(character)
            let convertedCharacter = NSMutableString(string: s)
            CFStringTransform(convertedCharacter, nil, kCFStringTransformStripCombiningMarks, false)
            if let c = UnicodeScalar(convertedCharacter as String) {
                if Self.vowels.contains(c) {
                    continue
                }
            }
            result.append(character)
        }

        return result
    }

    private static let vowels = CharacterSet(charactersIn: "aeiouy")
}
