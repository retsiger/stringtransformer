//
//  BinaryTransformer.swift
//
//  Abstract:
//  Returns the binary representation of UTF8 encoded text.
//
//  Created by Joël Brogniart on 08/07/2021.
//

import Foundation

struct BinaryTransformer: Transformer {
    let name = String.Transformer.binary

    func description() -> String {
        let description = String(localized: "binary-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the binary transformer.")
        return description
    }

    func isPure() -> Bool {
        return true
    }
    
    /**
     Create a binary representation of the original text.
     
     - Parameter text: Text to transform.
     - Returns: The transformed text.
     */
    func transform(_ text: String) -> String {
        var result = String()
        let bytes = Array(text.utf8)
        bytes.forEach {byte in
            var b = byte
            var binary = String()
            for _ in 0..<8 {
                let currentBit = b & 0x01
                if currentBit == 0 {
                    binary.append("0")
                } else {
                    binary.append("1")
                }
                b >>= 1
            }
            result.append(String(binary.reversed()))
        }
        return result
    }
}

