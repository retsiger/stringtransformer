//
//  MorseTransformer.swift
//
//  Created by Joël Brogniart on 30/06/2021.
//

import Foundation

struct MorseTransformer: Transformer, CharacterReplacer {
    let name = String.Transformer.morse

    func description() -> String {
        let description = String(localized: "morse-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the morse transformer.")
        return description
    }

    func isPure() -> Bool {
        return true
    }
    
    func transform(_ text: String) -> String {
        return characterToStringSpacedReplace(text.uppercased(), Self.morseCode)
    }

    /*
        Morse code taken from wikipedia.
        https://en.wikipedia.org/wiki/Morse_code
     */
    private static let morseCode: [Character: String] = [
        "A" : ".-",
        "B" : "-...",
        "C" : "-.-.",
        "D" : "-..",
        "E" : ".",
        "F" : "..-.",
        "G" : "--.",
        "H" : "....",
        "I" : "..",
        "J" : ".---",
        "K" : "-.-",
        "L" : ".-..",
        "M" : "--",
        "N" : "-.",
        "O" : "---",
        "P" : ".--.",
        "Q" : "--.-",
        "R" : ".-.",
        "S" : "...",
        "T" : "-",
        "U" : "..-",
        "V" : "...-",
        "W" : ".--",
        "X" : "-..-",
        "Y" : "-.--",
        "Z" : "--..",
        "0" : "-----",
        "1" : ".----",
        "2" : "..---",
        "3" : "...--",
        "4" : "....-",
        "5" : ".....",
        "6" : "-....",
        "7" : "--...",
        "8" : "---..",
        "9" : "----.",
        "." : ".-.-.-",
        "," : "--..--",
        "?" : "..--..",
        "'" : ".----.",
        "!" : "-.-.--",
        "/" : "-..-.",
        "(" : "-.--.",
        ")" : "-.--.-",
        "&" : ".-...",
        ":" : "---...",
        ";" : "-.-.-.",
        "=" : "-...-",
        "+" : ".-.-.",
        "-" : "-....-",
        "_" : "..--.-",
        "\"" : ".-..-.",
        "$" : "...-..-",
        "@" : ".--.-.",
        "ä" : ".-.-",
        "æ" : ".-.-",
        "ą" : ".-.-",
        "à" : ".--.-",
        "å" : ".--.-",
        "ć" : "-.-..",
        "ĉ" : "-.-..",
        "ç" : "-.-..",
        "ð" : "..--.",
        "đ" : "..-..",
        "é" : "..-..",
        "è" : ".-..-",
        "ę" : "..-..",
        "ĝ" : "--.-.",
        "ĥ" : "----",
        "ĵ" : ".---.",
        "ł" : ".-..-",
        "ń" : "--.--",
        "ñ" : "--.--",
        "ó" : "---.",
        "ö" : "---.",
        "ø" : "---.",
        "ś" : "...---...",
        "ŝ" : "...-.",
        "š" : "----",
        "þ" : ".--..",
        "ü" : "..--",
        "ŭ" : "..--",
        "ź" : "--..--.",
        "ż" : "--..-"
    ]
}
