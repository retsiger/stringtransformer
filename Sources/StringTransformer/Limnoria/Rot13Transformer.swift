//
//  Rot13Transformer.swift
//  
//
//  Created by Joël Brogniart on 20/07/2021.
//

import Foundation

struct Rot13Transformer: Transformer, CharacterReplacer {
    let name = String.Transformer.rot13

    func description() -> String {
        let description = String(localized: "rot13-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the rot13 transformer.")
        return description
    }

    func isPure() -> Bool {
        return true
    }
    
    func transform(_ text: String) -> String {
        return characterToCharacterReplace(text, Self.rot13Table)
    }
    
    private static let rot13Table: [Character: Character] = Dictionary(uniqueKeysWithValues: zip(Array("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), Array("nopqrstuvwxyzabcdefghijklmNOPQRSTUVWXYZABCDEFGHIJKLM")))
}
