//
//  CapWordsTransformer.swift
//  
//
//  Created by Joël Brogniart on 21/09/2021.
//

import Foundation

struct CapWordsTransformer: Transformer {
    let name = String.Transformer.capwords

    func isPure() -> Bool {
        return true
    }
    
    func description() -> String {
        let description = String(localized: "capwords-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the capwords transformer.")
        return description
    }
    
    func transform(_ text: String) -> String {
        if #available(macOS 10.11, iOS 9, *) {
            return text.localizedCapitalized
        } else {
            return text.capitalized
        }
    }
}
