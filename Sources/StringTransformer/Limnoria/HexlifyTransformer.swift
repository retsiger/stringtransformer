//
//  HexlifyTransformer.swift
//  
//
//  Created by Joël Brogniart on 20/07/2021.
//

import Foundation

struct HexlifyTransformer: Transformer {
    let name = String.Transformer.hexlify

    func description() -> String {
        let description = String(localized: "hexlify-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the hexlify transformer.")
        return description
    }

    func isPure() -> Bool {
        return true
    }
    
    func transform(_ text: String) -> String {
        var result = String()
        let bytes = Array(text.utf8)
        bytes.forEach {byte in
            result.append(String(format: "%2x", byte))
        }
        return result
    }
}

