//
//  UniudTransformer.swift
//  
//
//  Created by Joël Brogniart on 20/09/2021.
//

import Foundation

struct UniudTransformer: Transformer, CharacterReplacer {
    let name = String.Transformer.uniud

    func description() -> String {
        let description = String(localized: "uniud-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the uniud transformer.")
        return description
    }
    
    func isPure() -> Bool {
        return true
    }
    
    func transform(_ text: String) -> String {
        let transformed = characterToCharacterReplace(text, Self.turnedTable)
        return String(transformed.reversed())
    }
    
    static private let turnedTable: [Character: Character] = [
        // letters lowercase
        "a" : "ɐ",
        "à" : "ɐ",
        "b" : "q",
        "c" : "ɔ",
        "ç" : "ɔ",
        "d" : "p",
        "e" : "ǝ",
        "é" : "ǝ",
        "è" : "ǝ",
        "ê" : "ǝ",
        "ë" : "ǝ",
        "f" : "ɟ",
        "g" : "ɓ",
        "h" : "ɥ",
        "i" : "ᴉ",
        "î" : "ᴉ",
        "j" : "ɾ",
        "k" : "ʞ",
        "l" : "ꞁ",
        "m" : "ɯ",
        "n" : "u",
        "o" : "o",
        "ô" : "o",
        "p" : "d",
        "q" : "b",
        "r" : "ɹ",
        "s" : "s",
        "t" : "ʇ",
        "u" : "n",
        "ù" : "n",
        "û" : "n",
        "ü" : "n",
        "v" : "ʌ",
        "w" : "ʍ",
        "x" : "x",
        "y" : "ʎ",
        "z" : "z",
        // letters uppercase
        "A" : "∀",
        "B" : "ꓭ",
        "C" : "Ɔ",
        "D" : "ᗡ",
        "E" : "Ǝ",
        "F" : "Ⅎ",
        "G" : "⅁",
        "H" : "H",
        "I" : "I",
        "J" : "ᒋ",
        "K" : "Ʞ",
        "L" : "⅂",
        "M" : "Ɯ",
        "N" : "N",
        "O" : "O",
        "P" : "d",
        "Q" : "b",
        "R" : "ᴚ",
        "S" : "S",
        "T" : "⊥",
        "U" : "ᑎ",
        "V" : "Λ",
        "W" : "M",
        "X" : "X",
        "Y" : "⅄",
        "Z" : "Z",

        // digits
        "0" : "0",
        "1" : "1",
        //"2" : "\u2681",
        "3" : "Ɛ",
        //"4" : "\u2683",
        //"5" : "\u1515",
        "6" : "9",
        "7" : "L",
        "8" : "8",
        "9" : "6",
        " " : " ",
        "@" : "@",

        //symbols
        "!" : "¡",
        "\"" : "„",
        "&" : "⅋",
        "'": "‚",
        "(" : ")",
        ")" : "(",
        "," : "‘",
        "<" : ">",
        "." : "˙",
        ">" : "<",
        "?" : "¿",
        "`" : "ˎ",
        "[" : "]",
        "{" : "}",
        "|" : "|",
        "]" : "[",
        "}" : "{",
        "^" : "̬",
        "_" : "‾"
    ]
}
