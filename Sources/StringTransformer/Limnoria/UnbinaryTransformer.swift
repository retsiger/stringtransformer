//
//  UnbinaryTransformer.swift
//
//  Created by Joël Brogniart on 08/07/2021.
//

import Foundation

struct UnbinaryTransformer: Transformer {
    let name = String.Transformer.unbinary

    func description() -> String {
        let description = String(localized: "unbinary-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the unbinary transformer.")
        return description
    }

    func isPure() -> Bool {
        return true
    }
    
    func transform(_ text: String) -> String {
        // keep only 0 and 1 characters
        let binary = text.filter {"01".contains($0)}
        
        // Split input in 8 characters substrings and then convert substrings to int
        var utf8CodeUnits = [UInt8]()
        var startIndex = binary.startIndex
        while startIndex < binary.endIndex {
            let endIndex = binary.index(startIndex, offsetBy: 8, limitedBy: binary.endIndex) ?? binary.endIndex
            let byte = binary[startIndex..<endIndex]
            if let utf8CodeUnit = UInt8(byte, radix: 2) {
                utf8CodeUnits.append(utf8CodeUnit)
            }
            startIndex = endIndex
        }
        return String(decoding: utf8CodeUnits, as: UTF8.self)
    }
}
