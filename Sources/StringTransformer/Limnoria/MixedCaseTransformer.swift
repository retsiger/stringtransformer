//
//  MixedCaseTransformer.swift
//  
//
//  Created by Joël Brogniart on 20/01/2022.
//

import Foundation

struct MixedCaseTransformer: Transformer {
    let name = String.Transformer.mixedcase

    func description() -> String {
        let description = String(localized: "mixedcase-transformer-description",
                                 table: "Descriptions",
                                 bundle: .module,
                                 comment: "Description of the mixedcase transformer.")
        return description
    }

    func isPure() -> Bool {
        return false
    }
    
    func transform(_ text: String) -> String {
        let transformed = text.map {(c: Character) -> String in
            if #available(macOS 10.11, iOS 9, *) {
                return  Int.random(in: 0...1) == 1 ? c.description.localizedUppercase : c.description.localizedLowercase
            } else {
                return Int.random(in: 0...1) == 1 ? c.uppercased() : c.lowercased()
            }
        }
        return transformed.joined(separator: "")
    }
}
