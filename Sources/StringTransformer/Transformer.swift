//
//  Transformer.swift
//  StringTransformer
//
//  Created by Joël Brogniart on 05/11/2024.
//

import Foundation

/**
 A type that can transform a string to another string.
  
```swift
 struct ReverseTransformer: Transformer {
     let name = String.Transformer.reverse
      
     func description() -> String {
         let description = String(localized: "reverse-transformer-description",
                                  table: "Descriptions",
                                  bundle: .module,
                                  comment: "Description of the reverse transformer.")
         return description
     }

     func isPure() -> Bool {
         return true
     }
     
     func transform(_ text: String) -> String {
         return String(text.reversed())
     }
 }
 ```
 */

protocol Transformer: Hashable, Sendable {
    /**
     The name of the string transformer.
     
     The name is used to compare transformers. Two transformers having the same name are considered as the same transformer. It is used to acces to a specific transformer when transformers are grouped in a set.
     
     - Returns: The name of the transformer.
     */
    var name: String.Transformer {
        get
    }

    /**
     A short description of the transformer.
     */
    func description() -> String
    
    /**
     Return true if the transformer return always the same result for the same entry.
     
     A pure transformer is a transformer that always returns the same result for a given input. Calling a non pure transformer multiple times for the same string could return different results.
     */
    func isPure() -> Bool

    /**
     Transform a string.
     
     - Parameter text: Text to transform.
     - Returns: A transformation of ``text`` by the transformer.
     */
    func transform(_ text: String) -> String
}

extension Transformer {
    // Hashable
    func hash(into hasher: inout Hasher) {
        return hasher.combine(self.name)
    }
    
    // Equatable
    static func == (lhs: any Transformer, rhs: any Transformer) -> Bool {
        return lhs.name == rhs.name
    }
}

/**
 A type that can transform a string to another string.
 
 This structure is used as  a type eraser for a Transformer structure. It is Hashable and Equatable so Transformers could be added to a set.
 */
struct AnyTransformer: Transformer {
    private let wrapped: any Transformer
    
    public init(_ wrapped: any Transformer) {
        self.wrapped = wrapped
    }
        
    var name: String.Transformer {
        wrapped.name
    }

    func description() -> String {
        return wrapped.description()
    }
    
    func isPure() -> Bool {
        return wrapped.isPure()
    }
    
    func transform(_ text: String) -> String {
        return wrapped.transform(text)
    }
        
    // Equatable
    static func == (lhs: AnyTransformer, rhs: AnyTransformer) -> Bool {
        return lhs.name == rhs.name
    }
    
}
/**
 Set of Transformers
 */
extension Set where Element: Transformer {
    subscript(transformer: String.Transformer) -> (any Transformer)? {
        return self.first(where: { $0.name == transformer })
    }

    func allCases() -> [String.Transformer] {
        return map(\.name).sorted()
    }
    
    /**
     Indicate if a transformer is pure.

     - Parameter transformer: Name of the transformation whose purity is to be requested..
     - Returns: true if a transformer with that transformation is a pure transformer.
     */
    func isPure(_ transformer: String.Transformer) -> Bool {
        return self[transformer]?.isPure() ?? true
    }
    
    /**
     Give a short description of a transformation.
     
     - Parameter transformer: Transformation for which a description is to be requested.
     - Returns: The description of the transformer.
     */
    func description(for transformer: String.Transformer) -> String {
        return self[transformer]?.description() ?? ""
    }
    
    /**
     Transform a string using a specific transformation.
     
     - Parameter text: Text to transform.
     - Parameter transformer: transformer to use.
     - Returns: A transformation of ``text`` by the transformer.
     */
    func transform(_ text: String, with transformer: String.Transformer) -> String {
        return self[transformer]?.transform(text) ?? ""
    }
}
