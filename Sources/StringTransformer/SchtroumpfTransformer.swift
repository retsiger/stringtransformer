//
//  SchtroumpfTransformer.swift
//  StringTransformer
//
//  Created by Joël Brogniart on 18/11/2024.
//

import Foundation
import NaturalLanguage

struct SchtroumpfTransformer: Transformer, LanguageSpecific {
    let name = String.Transformer.schtroumpf
    
    func description() -> String {
        let description = String(localized: "schtroumpf-transformer-description",
                                 table: "Schtroumpf",
                                 bundle: .module,
                                 comment: "Description du transformateur schtroumpf.")
        return description
    }
    //    value: "Transforme le texte comme s'il avait été raconté par un schtroumpf  (https://fr.wikipedia.org/wiki/Les_Schtroumpfs).",
    
    func isPure() -> Bool {
        return false
    }
    
    func transform(_ text: String) -> String {
        let recognizer = NLLanguageRecognizer()
        recognizer.processString(text)
        // Well, let's code for a known (by me) language!
        if let language = recognizer.dominantLanguage, language == .french {
            let tagger = NLTagger(tagSchemes: [.lexicalClass, .lemma])
            tagger.string = text
            var elements = [String]()
            let options: NLTagger.Options = []
            var lastChange = -1
            var current = 0
            tagger.enumerateTags(in: text.startIndex..<text.endIndex, unit: .word, scheme: .lexicalClass, options: options) { tag, tokenRange in
                // Don't change punctuation
                if [.punctuation, .sentenceTerminator, .openQuote, .closeQuote, .openParenthesis, .closeParenthesis, .wordJoiner, .dash, .otherPunctuation, .whitespace, .paragraphBreak, .otherWhitespace, .other].contains(tag) {
                    elements.append(String(text[tokenRange]))
                    return true
                }
                current += 1
                let word = String(text[tokenRange])
                // Avoid two consecutive schtroumpf.
                if current == (lastChange + 1) {
                    elements.append(word)
                    return true
                }
                // Not all words should be replaced.
                if Int.random(in: 1...10) > 6 {
                    elements.append(word)
                    return true
                } else {
                    let upper = word.first?.isUppercase ?? false
                    var schtroumpf = String(localized: "schtroumpf",
                                            table: "Schtroumpf",
                                            bundle: .module,
                                            comment: "La base principale du vocabulaire schtroumpf.")
                    if upper {
                        schtroumpf = schtroumpf.localizedCapitalized
                    }
                    let lemmaTag = tagger.tag(at: tokenRange.lowerBound, unit: .word, scheme: .lemma)
                    let lemma = lemmaTag.0?.rawValue ?? ""
                    switch tag {
                    case .adverb:
                        if lemma.hasSuffix("ement") {
                            elements.append(String(localized:"\(schtroumpf)ement", table:"Haddock", bundle: .module))
                            lastChange = current
                        } else {
                            elements.append(word)
                        }
                    case .verb:
                        switch lemma {
                        case "être", "avoir": // No change for some verbs
                            elements.append(word)
                        case text[tokenRange].lowercased(): // Probably a verb in the infinitive form. Simpler to replace.
                            elements.append(String(localized:"\(schtroumpf)er", table:"Schtroumpf", bundle: .module))
                            lastChange = current
                        default:
                            elements.append(word)
                        }
                    case .noun:
                        // Attempt at detecting plural form.
                        if lemma == text[tokenRange].lowercased() {
                            elements.append(schtroumpf)
                        } else {
                            elements.append(String(localized:"\(schtroumpf)s", table:"Schtroumpf", bundle: .module))
                        }
                        lastChange = current
                    default:
                        elements.append(word)
                    }
                    return true
                }
            }
            var result = elements.joined()
            // More French specific treatment.
            result = schtroumpfUnElision(result)
            result = contraction(result)
            return result
        }
        return text
    }
}
