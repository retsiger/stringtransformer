//
//  StringTransformer.swift
//
//  Created by Joël Brogniart on 23/06/2021.
//
// Abstract:
// A String extension to offer string transformations.
//

import Foundation

/// Allows a raw enum type to be compared by the underlying comparable RawValue
public protocol RawComparable : Comparable where Self : RawRepresentable, RawValue: Comparable {
}

extension RawComparable {
    public static func < (lhs: Self, rhs: Self) -> Bool {
        return lhs.rawValue < rhs.rawValue
    }
}

/**
 A String extension that adds transformers.
*/
extension String {
    /**
     Transformations
     */
    public enum Transformer: String, CaseIterable, RawComparable, Sendable {
        case anonymous
        case anonymouslight
        case aol
        case binary
        case caps
        case capwords
        case circled
        case gnu
        case hebrew
        case hexlify
        case jeffk
        case leet
        case profanity
        case mixedcase
        case morse
        case reverse
        case rot13
        case schtroumpf
        case scramble
        case shrink
        case spellit
        case squish
        case supa1337
        case unbinary
        case undup
        case unhexlify
        case uniud
        case unmorse
        case uwu
        case vowelrot
    }

    /**
     The set of available transformers.
     */
    static let transformers: Set = [
        AnyTransformer(AnonymousTransformer()),
        AnyTransformer(AnonymousLightTransformer()),
        AnyTransformer(AOLTransformer()),
        AnyTransformer(BinaryTransformer()),
        AnyTransformer(CapsTransformer()),
        AnyTransformer(CapWordsTransformer()),
        AnyTransformer(CircledTransformer()),
        AnyTransformer(GNUTransformer()),
        AnyTransformer(HebrewTransformer()),
        AnyTransformer(HexlifyTransformer()),
        AnyTransformer(JeffKTransformer()),
        AnyTransformer(LeetTransformer()),
        AnyTransformer(LeoProfanityTransformer()),
        AnyTransformer(MixedCaseTransformer()),
        AnyTransformer(MorseTransformer()),
        AnyTransformer(ReverseTransformer()),
        AnyTransformer(Rot13Transformer()),
        AnyTransformer(SchtroumpfTransformer()),
        AnyTransformer(ScrambleTransformer()),
        AnyTransformer(ShrinkTransformer()),
        AnyTransformer(SpellItTransformer()),
        AnyTransformer(SquishTransformer()),
        AnyTransformer(Supa1337Transformer()),
        AnyTransformer(UnbinaryTransformer()),
        AnyTransformer(UndupTransformer()),
        AnyTransformer(UnhexlifyTransformer()),
        AnyTransformer(UniudTransformer()),
        AnyTransformer(UnmorseTransformer()),
        AnyTransformer(UwuTransformer()),
        AnyTransformer(VowelRotTransformer())
    ]
    
    /**
     Give the list of available transformers.
     
     - Returns: A sorted array with the available transformers.
     */
    public static let availableTransformers: [String.Transformer] = transformers.allCases()
    
    /**
     Indicate if a transformer is pure .
     
     A pure transformer is a transformer that always return the same result for a given input. Calling a non pure transformer multiple times for the same text could return different results.
     
     ```swift
     let isBinaryTransformationPure = String.isPure(.binary)
     // Should be true
     let isJeffKTransformationPure = String.isPure(.jeffk)
     // Should be false
     ```
     
     - Parameter transformer: The name of a transformer.
     - Returns: True if the transformer is pure otherwise false.
     */
    public static func isPure(_ transformer: String.Transformer) -> Bool {
        return transformers.isPure(transformer)
    }
    
    /**
     Return the description of a transformer.
     
     Put the description of a transformer in a constant.
     ```swift
     let rot13TransformerDescription = String.description(for: .rot13)
     ```
     
     - Parameter transformer: A transformer.
     - Returns: the description of the transformer.
     */
    public static func description(for transformer: String.Transformer) -> String {
        return transformers.description(for: transformer)
    }
    
    /**
     Using a transformer  to obtain a new string.
     
     Generate a new string with a transformer.
     ```swift
     let sentence = "There are no strangers, only friends you have not met yet."
     let jeffkSentence = sentence.transform(with: .jeffk)
     ```
     
     - Parameter transformer: The transformer that will be used.
     - Returns: A new string that is a transformation of the source string.
     */
    public func transform(with transformer: String.Transformer) -> String {
        return String.transformers.transform(self, with: transformer)
    }
}
